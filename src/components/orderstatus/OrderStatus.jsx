import React, { useState, useRef, useContext } from "react"
import { useParams } from "react-router-dom"
import { useCookies } from "react-cookie"
import {setLoading} from '../../redux/loadingDucks'

const OrderStatus = () => {

	const [order, setOrder] = React.useState([]);
	const [cookies, setCookie] = useCookies(["user"]);

  	const { id } = useParams();

  	React.useEffect(() => {
		getOrders();
	},[]);


	const getOrders = async () => {
		const data = await fetch(`${process.env.REACT_APP_URL_API}/api/sale`, {
			method: "POST",
			mode: "cors",
			headers: {
				"Content-Type": "application/json",
				"Accept" : "application/json",
			},
			body: JSON.stringify({
				id
			})
		});
		const db_order = await data.json();
		setOrder(db_order);
		console.log(db_order);
	};


	const getTitle = (state_id) =>{
		switch(state_id){
			case 1:
				return "Su compra fue creada con exito";
			break;
			case 2:
				return "su pago esta pendiente de confirmacion";
			break;
			case 3: 
				return "gracias por tu compra";
			break;
		}
	}

	const getMessage = (state_id) =>{
		switch(state_id){
			case 1:
				return "Su orden fue creada y esta lista para ser pagada";
			break;
			case 2:
				return "Su pedido fue recibido, pero aun no podemos confirmar su pago, si utilizo un medio de pago online esto podria tardar unos minutos";
			break;
			case 3: 
				return "Su pago fue procesado correctamente y estamos preparando su orden";
			break;
		}
	}

	const getIcon = (state_id) =>{
		switch(state_id){
			case 1:
				return "fa fa-exclamation-circle";
			break;
			case 2:
				return "fa fa-question-circle";
			break;
			case 3: 
				return "fa fa-check-circle";
			break;
		}
	}

	
	const getTransaction = (state_id, transaction_code) =>{
		switch(state_id){
			case 3: 
				return <p>Codigo de transacci&oacute;n: {transaction_code} </p>
			break;
		}
	}


	return (
		order.data ? 

					<section class="section-b-space light-layout">
        					<div class="container">
            						<div class="row">
                						<div class="col-md-12">
                    							<div class="success-text"><i class={getIcon(order.data[0].sale_state_id)} aria-hidden="true"></i>
                        							<h2>{getTitle(order.data[0].sale_state_id)}</h2>
                        							<p>{getMessage(order.data[0].sale_state_id)}</p>
										{getTransaction(order.data[0].sale_state_id, order.data[0].id)}
                        						</div>
                						</div>
							</div>
						</div>
					</section>
		:
		null
		)
	};

export default OrderStatus;

import React from 'react'
import product_img from './cart_images/pro3/1.jpg'
import { useHistory, Redirect } from "react-router"
import { useCookies } from "react-cookie"
import { useDispatch, useSelector } from 'react-redux'
import { deleteProductLS, updateProductLS, checkedProductLS } from '../../localStorage/cartLS'
import { setLoading } from '../../redux/loadingDucks'

const CartDetail = () => {

	const dispatch = useDispatch();
	let history = useHistory()
	const [cookies, setCookie] = useCookies(["user"])
	const [cart, setCart] = React.useState([])

	React.useEffect(() => {
		getCartProducts()
	}, [])

	const getCartProducts = async () => {
		dispatch(setLoading(1));
		if (cookies.access_token != null) {
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/cart`, {
				method: 'GET',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + cookies.access_token,
					'Accept': 'application/json',
				}
			});
			const db_cart = await data.json()
			setCart(db_cart)
		} else {
			let cartStorage = JSON.parse(localStorage.getItem('cart'))
			let newCart = []
			newCart['data'] = cartStorage
			setCart(newCart)
		}
		dispatch(setLoading(-1));
	}

	const deleteCartProduct = async (index) => {
		dispatch(setLoading(1));
		if (cookies.access_token != null) {
			if (window.confirm("\u00BF Seguro que desea eliminar el producto del carrito de compras ?")) {
				const newCart = Object.assign([], cart);
				const deletedCart = newCart.data.splice(index, 1);
				setCart(newCart);
				const id = deletedCart[0].id;
				const data = await fetch('http://127.0.0.1:8000/api/cart', {
					method: 'DELETE',
					mode: 'cors',
					cache: 'no-cache',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + cookies.access_token,
						'Accept': 'application/json',
					},
					body: JSON.stringify({
						id,
					}),
				});
				setCart(newCart)
			}
		} else {
			if (window.confirm("\u00BF Seguro que desea eliminar el producto del carrito de compras ?")) {
				deleteProductLS(cart.data[index].product)
				let cartStorage = JSON.parse(localStorage.getItem('cart'))
				let newCart = []
				newCart['data'] = cartStorage
				setCart(newCart)
			}
		}
		dispatch(setLoading(-1));
	}

	const updateCartProduct = async (event, index) => {
		dispatch(setLoading(1));
		if (cookies.access_token != null) {
			if (window.confirm("\u00BF Seguro que desea cambiar la cantidad de productos del carrito de compras ?")) {
				const SelectedCartNew = Object.assign([], cart)
				const id = event.target.id
				let selected = false
				const qty = event.target.value

				for (let i = 0; i < SelectedCartNew.data.length; i++) {
					if (id == SelectedCartNew.data[i].id) {
						SelectedCartNew.data[i].qty = qty
						selected = SelectedCartNew.data[i].selected
					};

				}

				const data = await fetch(`${process.env.REACT_APP_URL_API}/api/cart`, {
					method: 'PATCH',
					mode: 'cors',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + cookies.access_token,
						'Accept': 'application/json',
					},
					body: JSON.stringify({
						id,
						qty,
						selected,
					}),
				});
				const cartItemUpdated = await data.json();
				if (cartItemUpdated.status == "false") {
					window.alert(cartItemUpdated.message)
					getCartProducts()
				} else {
					getCartProducts()
				}
			}
		} else {
			const qty = event.target.value
			updateProductLS(index, qty)
			let cartStorage = JSON.parse(localStorage.getItem('cart'))
			let newCart = []
			newCart['data'] = cartStorage
			setCart(newCart)
		}
		dispatch(setLoading(-1));
	}

	const UpdateSelectedCartProduct = async (event, index) => {
		dispatch(setLoading(1));
		if (cookies.access_token != null) {
			const SelectedCartNew = Object.assign([], cart)
			const id = event.target.id
			const selected = event.target.checked
			let qty = 0

			for (let i = 0; i < SelectedCartNew.data.length; i++) {
				if (id == SelectedCartNew.data[i].id) {
					SelectedCartNew.data[i].selected = selected
					qty = SelectedCartNew.data[i].qty
				};
			}
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/cart`, {
				method: 'PATCH',
				mode: 'cors',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + cookies.access_token,
					'Accept': 'application/json',
				},
				body: JSON.stringify({
					id,
					qty,
					selected,
				}),
			})
			const cartItemUpdated = await data.json();
			if (cartItemUpdated.status == "false") {
				window.alert(cartItemUpdated.message)
				getCartProducts()
			} else {
				getCartProducts()
			}
		} else {
			const selected = event.target.checked
			checkedProductLS(index, selected)
			let cartStorage = JSON.parse(localStorage.getItem('cart'))
			let newCart = []
			newCart['data'] = cartStorage
			setCart(newCart)
			console.log(newCart)
		}
		dispatch(setLoading(-1));
	}

	const redirectProductList = () => {
		history.push('/productlist')
	}

	const redirectProduct = (productId) => {
		history.push('/product/' + productId)
	}

	const redirectPayment = (e) => {
		e.preventDefault();
		console.log(cart)
		let contador = 0;
		if(cart.data.length > 0){
			for(let x = 0; x < cart.data.length; x++){
				if(cart.data[x].selected == 1){
					contador++;
				}
			}
		}
		if(cart.data.length == 0){
			alert("El carrito esta vacio");
			return;
		}
		if(contador == 0){
			alert("Seleccione uno o varios productos del carrito para pagar.");
			return;
		}
		
		if(cookies.access_token != null){
			if(cart.data.length > 0){
				history.push('/payment')
			} else {
				alert("El carrito esta vacio");
			}	
		} else {
			if(cart.data.length > 0){
				history.push('/accountPayment')
			} else {
				alert("El carrito esta vacio");
			}		
		}

	}

	const validarOferta = (isOffer) => {
		if ((isOffer.invert == 0) && (isOffer.y > 0 || isOffer.m > 0 || isOffer.d > 0 || isOffer.h > 0 || isOffer.i > 0)) {
			return 1
		}
		return 0
	}

	const getTotalItemPrice = (cart) => {
		let total = 0
		if (validarOferta(cart.product.isOffer)) {
			total = cart.product.special_price * cart.qty
		} else {
			total = cart.product.price * cart.qty
		}
		return numberWithPoints(total)
	}

	const getTotalPrice = () => {
		let total = 0
		if (cart.data) {
			for (let x = 0; x < cart.data.length; x++) {
				if (validarOferta(cart.data[x].product.isOffer)) {
					total += cart.data[x].product.special_price * cart.data[x].qty
				} else {
					total += cart.data[x].product.price * cart.data[x].qty
				}
			}
		}

		return numberWithPoints(total)
	}

	const numberWithPoints = (number) => {
		return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
	}

	return (

		//section start
		<section className="cart-section section-b-space">
			<div className="container">
				<div className="row">
					<div className="col-sm-12">
						<table className="table cart-table table-responsive-md">
							<thead>
								<tr className="table-head">
									<th scope="col">...</th>
									<th scope="col">imagen</th>
									<th scope="col">nombre producto</th>
									<th scope="col">precio</th>
									<th scope="col">cantidad</th>
									<th scope="col">acci&oacute;n</th>
									<th scope="col">total</th>
								</tr>
							</thead>
							<tbody>
								{
									cart.data && cart ?
										cart.data.length > 0 ? cart.data.map((thiscart, index) => (
											<tr key={thiscart.id}>
												{/* START CHECKBOX */}
												<td scope="row">
													<input
														type="checkbox"
														checked={thiscart.selected}
														id={thiscart.id}
														onChange={(e) => UpdateSelectedCartProduct(e, index)} />
												</td>
												{/* END CHECKBOX */}
												<td>
													{
														thiscart.product.product_images && thiscart.product.product_images.length > 0 ?
															<a onClick={() => redirectProduct(thiscart.product.id)} href=""><img src={thiscart.product.product_images[0].url} alt=""></img></a>
															:
															<a onClick={() => redirectProduct(thiscart.product.id)} href=""><img src={process.env.REACT_APP_URL_API + '/image/no_disponible.png'} alt=""></img></a>
													}

												</td>
												<td>
													<a href="#">{thiscart.product.name}</a>
													<div className="mobile-cart-content row">
														<div className="col-xs-3">
															<div className="qty-box">
																<div className="input-group">
																	<input type="text" name="quantity" className="form-control input-number" defaultValue={thiscart.qty}></input>
																</div>
															</div>
														</div>
														<div className="col-xs-3">
															{
																validarOferta(thiscart.product.isOffer) == 1 ?
																	<h2 className="td-color">{thiscart.product.formated_special_price} <del>${thiscart.product.formated_price}</del></h2>
																	: <h2 className="td-color">{thiscart.product.formated_price}</h2>
															}
														</div>
														<div className="col-xs-3">
															<h2 className="td-color"><a href="#" className="icon"><i className="ti-close"></i></a>
															</h2>
														</div>
													</div>
												</td>
												<td>
													{
														validarOferta(thiscart.product.isOffer) == 1 ?
															<h2>$ {thiscart.product.formated_special_price} <del>${thiscart.product.formated_price}</del></h2> :
															<h2>$ {thiscart.product.formated_price}</h2>
													}

												</td>
												<td>
													<div className="qty-box">
														<div className="input-group">
															<input type="number"
																id={thiscart.id}
																onChange={(e) => updateCartProduct(e, index)}
																name="quantity"
																className="form-control input-number"
																value={thiscart.qty} />
														</div>
													</div>
												</td>
												<td>
													<a href="#" onClick={deleteCartProduct.bind(this, index)} className="icon"><i className="ti-close"></i></a>
												</td>
												<td>
													<h2 className="td-color">${getTotalItemPrice(thiscart)}</h2>
												</td>
											</tr>


										))
											:
											<tbody>
												<tr>
													<td colSpan="6">
														<h1>no hay productos en el carrito</h1>
													</td>
												</tr>
											</tbody>
										:
										<tbody>
											<tr>
												<td colSpan="6">
													<h1>no hay productos en el carrito</h1>
												</td>
											</tr>
										</tbody>
								}
							</tbody>
						</table>
						<table className="table cart-table table-responsive-md">
							<tfoot>
								<tr>
									<td>total price :</td>
									<td>
										<h2>${getTotalPrice()}</h2>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<div className="row cart-buttons">
					<div className="col-6" onClick={redirectProductList}><a href="" className="btn btn-solid">continuar comprando</a></div>
					<div className="col-6" onClick={redirectPayment}><a href="" className="btn btn-solid">pagar</a></div>
				</div>
			</div>
		</section>
		//section end
	)

}

export default CartDetail;

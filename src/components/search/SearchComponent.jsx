import React,  { useState, useEffect } from 'react'
import icon_image from "./assets/images/search.png"

const SearchComponent = () => {

    function openSearch() {
        document.getElementById("search-overlay").style.display = "block";
    }

    function closeSearch() {
        document.getElementById("search-overlay").style.display = "none";
    }

    const [products, setProducts] = React.useState([])
    const [input, setInput] = useState('');
    useEffect(() => {

        const getOptionsFeatured = async () => {
            const data = await fetch('http://127.0.0.1:8000/api/product?search=' + input + '&limit=3', {
                method: 'OPTIONS',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            const prods = await data.json()
            
            if(prods.data){
                setProducts(prods);
            }
        }
        getOptionsFeatured();
    
    });
    


    return (

        <div>
            <li className="onhover-div mobile-search">
            <div>
                <img src={icon_image} onClick={()=> openSearch()} className="img-fluid blur-up lazyload" alt=""/>
                <i className="ti-search" onClick={()=> openSearch()}></i> 
            </div>

            <div id="search-overlay" className="search-overlay">
                <div> <span className="closebtn" onClick={() => closeSearch()} title="Close Overlay">×</span>
                    <div className="overlay-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-xl-12">
                                    <form>
                                        <div className="form-group">
                                            <input type="text" className="form-control" 
                                            id="search" placeholder="Search a Product"  onChange={ e  => setInput(e.target.value )}/>
                                        </div>
                                        <button type="submit" className="btn btn-primary"><i className="fa fa-search"></i></button>
                                    </form>
                                    
                                    <div id="Productos" className="container">
                                        <div className="row justify-content-center">
                                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </li>
        </div>
    )
}

export default SearchComponent
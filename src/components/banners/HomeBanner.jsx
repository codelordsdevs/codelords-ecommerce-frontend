import React, { Fragment } from 'react'
import Slider from 'react-slick';
import Link from 'next/link';

const HomeBanner = () => {

    return (
        <Fragment>
            <section className="p-0">
                <Slider className="slide-1 home-slider slick-initialized slick-slider">
                <div>
                    <div style={{backgroundImage: `url(${process.env.REACT_APP_URL_API}/image/carousel1.png)`}} className="home home1 text-center">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="slider-contain">
                                        <div>
                                            <h4>Welcome To Fashion</h4>
                                            <h1 className="border-font">Men Fashion</h1>
                                            <Link href=""> 
                                                <a className="btn btn-solid">Shop Now</a>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style={{backgroundImage: `url(${process.env.REACT_APP_URL_API}/image/carousel2.png)`}}  className="home home2 text-center">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="slider-contain">
                                        <div>
                                            <h4>Welcome To Fashion</h4>
                                            <h1 className="border-font">Woman Fashion</h1>
                                            <Link href=""> 
                                                <a className="btn btn-solid">Shop now</a>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </Slider>
            </section>
        </Fragment>
    )
}

export default HomeBanner

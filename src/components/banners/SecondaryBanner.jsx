import React, { Fragment } from 'react';

const SecondaryBanner = () => {
    return (
        <Fragment>
            <section className="p-0">
                <div style={{backgroundImage: `url(${process.env.REACT_APP_URL_API}/image/parallax.png)`}} className="full-banner parallax-banner1 parallax text-center p-left">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <div className="banner-contain">
                                    <h2>2021</h2>
                                    <h3 className="border-font">fashion trends</h3>
                                    <h4>special offer</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}

export default SecondaryBanner

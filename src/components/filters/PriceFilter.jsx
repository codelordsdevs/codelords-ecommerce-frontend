import React from 'react'

import {useDispatch, useSelector} from 'react-redux'
import InputRange from 'react-input-range'
import 'react-input-range/lib/css/index.css'

import {changePriceRange, getProductFiltered} from '../../redux/productDucks'

const PriceFilter = () => {

    const dispatch = useDispatch()

    const [priceRange, setPriceRange] = React.useState({ min: 0, max: 1000000 })

    const changePriceRangef = (newPriceRange) => {
        setPriceRange(priceRange)
        dispatch(changePriceRange(priceRange.min, priceRange.max))
        dispatch(getProductFiltered())
    }

    return (
        <div className="collection-collapse-block border-0 open">
            <h3 className="collapse-block-title">Precio</h3>
            <div className="collection-collapse-block-content">
                <div className="wrapper mt-3">
                    <div className="range-slider">
                        <InputRange
                            maxValue={1000000}
                            minValue={0}
                            value={priceRange}
                            onChange={priceRange => setPriceRange(priceRange)}
                            onChangeComplete={priceRange => changePriceRangef(priceRange)} 
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PriceFilter

import React from 'react'
import axios from 'axios'

import {useDispatch} from 'react-redux'

import {addCategoryFilter, deleteCategoryFilter, getProductFiltered} from '../../redux/productDucks'

const CategoryFilter = () => {

    const dispatch = useDispatch()

    const [categorys, setCategorys] = React.useState([])

    React.useEffect(() => {
        getAllCategorys()
    }, [])

    const getAllCategorys = async () => {
        try {
            const res = await axios.get(`${process.env.REACT_APP_URL_API}/api/category`)
            setCategorys(res.data.data)
            
        } catch(error) {
            console.log(error)
        }
    }

    const checkCategory = (event) => {
        if(event.target.checked){
            dispatch(addCategoryFilter(event.target.name))
            dispatch(getProductFiltered())
        } else {
            dispatch(deleteCategoryFilter(event.target.name))
            dispatch(getProductFiltered())
        }
        
    }
    
    return (
        <div className="collection-collapse-block open">
            <h3 className="collapse-block-title">Categorías</h3>
            <div className="collection-collapse-block-content">
                <div className="collection-brand-filter">
                    {
                        categorys ?
                            categorys != null && categorys.length > 0 ?
                                categorys.map(item => (
                                    <div key={item.id} className="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" className="custom-control-input" id={item.name} name={item.id} onChange={checkCategory}></input>
                                        <label className="custom-control-label" htmlFor={item.name}>{item.name}</label>
                                    </div>
                                ))
                            : null
                        : null
                    }
                </div>
            </div>
        </div>
    )
}

export default CategoryFilter

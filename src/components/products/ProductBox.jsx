import React from 'react'
import { useHistory, Link } from 'react-router-dom'
import { toast } from 'react-toastify';
import { addProductLS } from '../../localStorage/cartLS'
import { useCookies } from "react-cookie"
import { getHtmlToast } from '../../redux/cartDucks'

const ProductBox = (props) => {
    
    const [hoy, setHoy] = React.useState([new Date()])
    const [cookies, setCookie] = useCookies(["user"])
    const [addedProd, setAddedProd] = React.useState()

    const validarOferta = (isOffer) => {
        if((isOffer.invert == 0) && (isOffer.y > 0 || isOffer.m > 0 || isOffer.d > 0 || isOffer.h > 0 || isOffer.i > 0)){
            return 1
        }
        return 0
    }

    const history = useHistory()

    const addProduct = async () => {
        let product = {
            id: props.id,
            name: props.name,
            price: props.price,
            stock: props.stock,
            description: props.description,
            featured: props.featured,
            category_id: props.category_id,
            special_price: props.special_price,
            special_price_end_date: props.special_price_end_date,
            product_images: props.product_images,
            isOffer: props.isOffer,
            formated_price: props.formated_price,
            formated_special_price: props.formated_special_price
        }
		if(cookies.access_token != null){
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/cart?product_id=${product.id}&qty=${1}&user_id=1`,{
				method: "PUT",
				mode: "cors",
				cache: "no-cache",
				headers:{
				"Content-Type": "application/json",
				'Authorization' : 'Bearer '+cookies.access_token,
				'Accept' : 'application/json',
				},
				});
				const addProd = await data.json();
				setAddedProd(addProd);
				toast(getHtmlToast(product))
		} else {
			addProductLS(product)
			// dispatch(addProductInCartAction(product.data[0]))
		}
	}
    
    return (
        <div className="product-box">
            <div className="img-wrapper">
                <div className="lable-block">
                    {/* <span class="lable3">new</span>  */}
                    {
                        validarOferta(props.isOffer) ? <span className="lable4">Oferta</span> : null
                    }
                </div>
               
		{
		    props.product_images ?
		    	props.product_images.length > 0 ?
			    <>
                    <div className="front">
                        <Link to={`/product/${props.id}`}                        
                            className="bg-size blur-up lazyloaded" 
                            style={
                                {
                                    "backgroundImage": `url(${props.product_images[0].url})`, 
                                    "backgroundSize": "cover", 
                                    "backgroundPosition": 
                                    "center center", 
                                    "display": "block"
                                }
                            }
                        >
                            <img 
                                src={props.product_images[0].url} 
                                className="img-fluid blur-up lazyload bg-img" 
                                alt="" 
                                style={{display: "none"}}
                            />
                        </Link>	
                    </div>
                    <div className="back" >
                        <Link to={`/product/${props.id}`}  
                        href="" 
                        className="bg-size blur-up lazyloaded" 
                        style={
                            {
                                backgroundImage:  `url(${props.product_images[0].url})`, 
                                backgroundSize: "cover",
                                backgroundPosition: "center center",
                                display: "block"
                            }
                        }
                        >
                        <img 
                            src={process.env.REACT_APP_URL_API+'/image/no_disponible.png'}  
                            className="img-fluid blur-up lazyload bg-img" 
                            alt="" 
                            style={{display: "none"}}/>
                        </Link>
                    </div>
                </>
	    		:
			    <>
                    <div className="front">
                        <Link to={`/product/${props.id}`}                        
                            className="bg-size blur-up lazyloaded" 
                            style={
                                    {
                                        "backgroundImage": `url(${process.env.REACT_APP_URL_API}/image/no_disponible.png)`, 
                                        "backgroundSize": "cover", 
                                        "backgroundPosition": 
                                        "center center", 
                                        "display": "block"
                                    }
                                    }
                        >
                            <img 
                                src={process.env.REACT_APP_URL_API+'/image/no_disponible.png'} 
                                className="img-fluid blur-up lazyload bg-img" 
                                    alt="" 
                                style={{display: "none"}}
                            />
                        </Link>
                    </div>
                    <div className="back" >
                        <Link to={`/product/${props.id}`}  
                        href="" 
                        className="bg-size blur-up lazyloaded" 
                        style={
                            {
                                backgroundImage: `url(${process.env.REACT_APP_URL_API}/image/no_disponible.png)`, 
                                backgroundSize: "cover",
                                backgroundPosition: "center center",
                                display: "block"
                            }
                        }
                        >
                            <img 
                            src={process.env.REACT_APP_URL_API+'/image/no_disponible.png'}  
                            className="img-fluid blur-up lazyload bg-img" 
                            alt="" 
                            style={{display: "none"}}/>
                        </Link>
                    </div>
                </>
                :
                <>
                    <div className="front">
                        <Link to={`/product/${props.id}`}                        
                            className="bg-size blur-up lazyloaded" 
                            style={
                                    {
                                        "backgroundImage": `url(${process.env.REACT_APP_URL_API}/image/no_disponible.png)`, 
                                        "backgroundSize": "cover", 
                                        "backgroundPosition": 
                                        "center center", 
                                        "display": "block"
                                    }
                                    }
                        >
                            <img 
                                src={process.env.REACT_APP_URL_API+'/image/no_disponible.png'} 
                                className="img-fluid blur-up lazyload bg-img" 
                                    alt="" 
                                style={{display: "none"}}
                            />
                        </Link>
                    </div>
                    <div className="back" >
                        <Link to={`/product/${props.id}`}  
                        href="" 
                        className="bg-size blur-up lazyloaded" 
                        style={
                            {
                                backgroundImage: `url(${process.env.REACT_APP_URL_API}/image/no_disponible.png)`, 
                                backgroundSize: "cover",
                                backgroundPosition: "center center",
                                display: "block"
                            }
                        }
                        >
                        <img 
                            src={process.env.REACT_APP_URL_API+'/image/no_disponible.png'}  
                            className="img-fluid blur-up lazyload bg-img" 
                            alt="" 
                            style={{display: "none"}}/>
                        </Link>
                    </div>
                </>
		}
                <div className="cart-info cart-wrap">
                    <button onClick={() => addProduct()}
                        data-toggle="modal" 
                        data-target="#addtocart" 
                        title="Agregar a carrito de compras">
                        <i className="ti-shopping-cart"></i>
                    </button>
                    {/* <a 
                        href="" 
                        title="Agregar a lista de deseos">
                        <i 
                            className="ti-heart" 
                            aria-hidden="true"></i>
                    </a> */}
                    <Link to={`/product/${props.id}`}   
                        href="" 
                        data-toggle="modal" 
                        data-target="#quick-view" 
                        title="Ver producto">
                        <i className="ti-search" aria-hidden="true"></i>
                    </Link> 
                    {/* <a 
                        href="" 
                        title="Compare">
                        <i className="ti-reload" aria-hidden="true"></i>
                    </a> */}
                </div>
            </div>
            <div className="product-detail">
                <div className="rating">
                    <i className="fa fa-star"></i> 
                    <i className="fa fa-star"></i> 
                    <i className="fa fa-star"></i> 
                    <i className="fa fa-star"></i> 
                    <i className="fa fa-star"></i>
                </div>
                <a href="">
                    <h6>{props.name}</h6>
                </a>
                { 
                    validarOferta(props.isOffer) == 1 ? 
                    <h4>${props.formated_special_price} <del>${props.formated_price}</del></h4> : 
                    <h4>${props.formated_price}</h4>
                }
                {/* <ul className="color-variant">
                    <li className="bg-light0"></li>
                    <li className="bg-light1"></li>
                    <li className="bg-light2"></li>
                </ul> */}
            </div>
        </div>
        // <div className="col-md-4">
        //     <div className="card mb-4 shadow-sm card-height">
        //         <div className="card-body">
        //             <h5>{props.name}</h5>
        //             <p className="card-text">
        //                 {
        //                     props.description.length > 35 ? 
        //                     props.description.substr(0, 35) + '...' :
        //                     props.description
        //                 }
        //             </p>
        //             <div className="d-flex justify-content-between align-items-center align-text-bottom">
        //                 <div className="btn-group">
        //                     <button 
        //                         type="button" 
        //                         className="btn btn-outline-secondary"
        //                         data-tip="Abrir producto">
        //                         <ReactTooltip />
        //                         <i className="bi bi-zoom-in"></i>
        //                     </button>
        //                     <button 
        //                         type="button" 
        //                         className="btn btn-outline-secondary"
        //                         data-tip="Agregar a carrito de compras">
        //                         <ReactTooltip />
        //                         <i className="bi bi-cart-plus"></i>
        //                     </button>
        //                 </div>
        //                 <small className="text-muted">${props.price}</small>
        //             </div>
        //         </div>
        //     </div>
        // </div>
    )
}

export default ProductBox

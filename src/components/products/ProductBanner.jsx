import React from 'react'

const ProductBanner = () => {
    return (
        <div className="top-banner-wrapper">
            <div className="img-ldr-top">
                <img src={`${process.env.REACT_APP_URL_API}/image/products.png`} className="img-fluid blur-up lazyloaded" alt=""></img>
            </div>
            <div className="top-banner-content small-section">
                <h4>Fashion</h4>
                <h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
        </div>
    )
}

export default ProductBanner

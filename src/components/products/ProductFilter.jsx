import React from 'react'
import CategoryFilter from '../filters/CategoryFilter'
import PriceFilter from '../filters/PriceFilter'

const ProductFilter = () => {
    return (
        <div className="col-sm-3 collection-filter">
            <div className="collection-filter-block">
                <div className="collection-mobile-back">
                    <span className="filter-back">
                        <i className="fa fa-angle-left" aria-hidden="true"></i> 
                        Atrás
                    </span>
                </div>
                <CategoryFilter />
                <PriceFilter />
            </div>
        </div>
    )
}

export default ProductFilter

import React from 'react'

import {useDispatch, useSelector} from 'react-redux'

import {getAllProductsAction, expandLimit} from '../../redux/productDucks'
import ProductBanner from './ProductBanner'
import ProductBox from './ProductBox'

const List = () => {

    const dispatch = useDispatch()

    const products = useSelector(store => store.product.array)

    const limit = useSelector(store => store.product.limit)

    React.useEffect(() => {
        dispatch(getAllProductsAction())
    }, [dispatch])

    return (
        <div className="collection-content col">
            <div className="page-main-content">
                <div className="row">
                    <div className="col-sm-12">
                        <ProductBanner />
                        {/* Same as */}
                        <div className="collection-product-wrapper">
                            <div className="product-wrapper-grid" style={{opacity: 1}}>
                                <div className="product-top-filter">
                                    <div className="row">
                                        <div className="col-xl-12">
                                            <div className="filter-main-btn">
                                                <span className="filter-btn btn btn-theme">
                                                    <i className="fa fa-filter" aria-hidden="true"></i> 
                                                    Filtros
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row margin-res">
                                    {
                                        products ? 
                                        products.length > 0 ? products.map((item, index) => (
                                            index < limit ? 
                                                <div className="col-lg-3" key={item.id}>
                                                    <ProductBox 
                                                        id={item.id} 
                                                        name={item.name} 
                                                        price={item.price} 
                                                        stock={item.stock} 
                                                        description={item.description} 
                                                        featured={item.featured} 
                                                        category_id={item.category_id} 
                                                        special_price={item.special_price} 
                                                        special_price_end_date={item.special_price_end_date}
                                                        product_images={item.product_images}
                                                        isOffer={item.isOffer}
                                                        formated_price={item.formated_price}
                                                        formated_special_price={item.formated_special_price}
                                                    />
                                                </div>
                                            : null
                                            )) :
                                            null
                                        :
                                        null
                                    }
                                </div>
                            </div>
                        </div>
                        {/* {
                            products.map(item => (
                                <li key={item.id}> {item.name} </li>
                            ))
                        } */}
                    </div>
                </div>
                {
                    products ?
                        products.length > limit ? 
                        <div className="small-section border-section border-top-0">
                            <div className="d-flex justify-content-center">
                                <button type="submit" className="btn btn-solid" id="mc-submit" onClick={ () => dispatch(expandLimit(limit)) }>Ver más</button>
                            </div>
                        </div>
                        : null
                    : null
                }
            </div>
        </div>
    )
}

export default List

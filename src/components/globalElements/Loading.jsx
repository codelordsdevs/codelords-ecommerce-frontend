import React from 'react'
import {useSelector} from 'react-redux'

const Loading = () => {
    const loading = useSelector(store => store.loading.loading)

    return (
        <div>
            {
                loading > 0 ?
                    <div className="loading"></div>
                : null
            }
        </div>
    )
}

export default Loading

import React from 'react'
import {useSelector} from 'react-redux'
import {Link} from "react-router-dom"

const Footer = () => {

    const configGlobal = useSelector(store => store.config)

    return (
        <footer className="footer-light">
            {/* <div className="light-layout">
                <div className="container">
                    <section className="small-section border-section border-top-0">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="subscribe">
                                    <div>
                                        <h4>KNOW IT ALL FIRST!</h4>
                                        <p>Never Miss Anything From Multikart By Signing Up To Our Newsletter.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <form action="" className="form-inline subscribe-form auth-form needs-validation" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">
                                    <div className="form-group mx-sm-3">
                                        <input type="text" className="form-control" name="EMAIL" id="mce-EMAIL" placeholder="Enter your email" required="required"></input>
                                    </div>
                                    <button type="submit" className="btn btn-solid" id="mc-submit">subscribe</button>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div> */}
            <section className="section-b-space light-layout">
                <div className="container">
                    <div className="row footer-theme partition-f">
                        <div className="col-lg-4 col-md-6">
                            <div className="footer-title footer-mobile-title">
                                <h4>about</h4>
                            </div>
                            <div className="footer-contant">
                                <div className="footer-logo d-flex justify-content-center">
                                    <img width="100" src={ configGlobal.one.url_logo } alt=""></img>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                                <div className="footer-social">
                                    <ul>
                                        <li><a href={configGlobal.one.url_facebook} target="_blank"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href={configGlobal.one.url_twitter} target="_blank"><i className="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href={configGlobal.one.url_instagram} target="_blank"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        <li><a href={ "https://wa.me/" + configGlobal.one.whatsapp_number } target="_blank"><i className="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                        <li><a href={configGlobal.one.url_linkedin} target="_blank"><i className="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col offset-xl-1">
                            <div className="sub-title">
                                <div className="footer-title">
                                    <h4>Tienda</h4>
                                </div>
                                <div className="footer-contant">
                                    <ul>
                                        <li><Link to="/" >Inicio</Link></li>
                                        <li><Link to="/productlist" >Productos</Link></li>
                                        {/* <li><a href="">clothing</a></li>
                                        <li><a href="">accessories</a></li>
                                        <li><a href="">featured</a></li> */}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* <div className="col">
                            <div className="sub-title">
                                <div className="footer-title">
                                    <h4>why we choose</h4>
                                </div>
                                <div className="footer-contant">
                                    <ul>
                                        <li><a href="https://sublimonkeys.ticroom.cl/front-end/index.html#">shipping &amp; return</a></li>
                                        <li><a href="https://sublimonkeys.ticroom.cl/front-end/index.html#">secure shopping</a></li>
                                        <li><a href="https://sublimonkeys.ticroom.cl/front-end/index.html#">gallary</a></li>
                                        <li><a href="https://sublimonkeys.ticroom.cl/front-end/index.html#">affiliates</a></li>
                                        <li><a href="https://sublimonkeys.ticroom.cl/front-end/index.html#">contacts</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div> */}
                        <div className="col">
                            <div className="sub-title">
                                <div className="footer-title">
                                    <h4>store information</h4>
                                </div>
                                <div className="footer-contant">
                                    <ul className="contact-list">
                                        <li><i className="fa fa-map-marker"></i>{configGlobal.one.address}</li>
                                        <li><i className="fa fa-phone"></i>Call Us: {configGlobal.one.phone_number}</li>
                                        <li><i className="fa fa-envelope-o"></i>Email Us: {configGlobal.one.email_contact}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div className="sub-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-xl-6 col-md-6 col-sm-12">
                            <div className="footer-end">
                                <p><i className="fa fa-copyright" aria-hidden="true"></i> 2020-22 themeforest powered by
                            pixelstrap</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer

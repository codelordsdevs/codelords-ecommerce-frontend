import React from "react";
import GetTotalPrice from "../getTotalPrice/GetTotalPrice"
import GetDesc from "../getTotalPrice/GetDesc"
import { useCookies } from "react-cookie";
import { Redirect } from "react-router";
import { useDispatch } from 'react-redux';
import { setLoading } from '../../redux/loadingDucks';
import DepositInfo from './DepositInfo';

const CheckoutDetail = (props) => {

    const dispatch = useDispatch();
    const [cookies, setCookie] = useCookies(["user"]);
    const [pickup, setPickUp] = React.useState(0);
    const [payment, setPayment] = React.useState(2);

    const PaymentProducts = async (e) => {
        dispatch(setLoading(1));
        if (props.receiver_name == '') {
            dispatch(setLoading(-1));
            return alert('El nombre y el apellido son campos obligatorios para la compra.');
        }
        if (props.phone == '') {
            dispatch(setLoading(-1));
            return alert('El telefono es un campo obligatorio para la compra.');
        }
        if (props.email == '') {
            dispatch(setLoading(-1));
            return alert('El email es un campo obligatorio para la compra.');
        } else {
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(props.email)) {
                dispatch(setLoading(-1));
                return alert('Debe ingresar un correo electronico valido');
            }
        }
        if (pickup == 0) {
            if (!props.city_id > 0) {
                dispatch(setLoading(-1));
                return alert('Para programar un envio con despacho debe ingresar la comuna');
            }
            if (!props.address != '') {
                dispatch(setLoading(-1));
                return alert('Para programar un envio con despacho debe ingresar la direccion');
            }
        }


        const payment_id = payment;
        const postal_code = props.postal_code;
        const address = props.address;
        const receiver_name = props.receiver_name;
        const phone = props.phone;
        const email = props.email;
        const city_id = props.city_id;
        const save_shipment = props.save_shipment;
        const name = props.shipment_name;


        const data = await fetch(`${process.env.REACT_APP_URL_API}/api/sale`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + cookies.access_token,
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                payment_id,
                postal_code,
                address,
                receiver_name,
                phone,
                email,
                city_id,
                pickup,
                save_shipment,
                name,
            }),
        });

        const response = await data.json();
        if (response.status == "true") {
            if (!response.SaleID > 0) {
                window.location.href = response.url;
            } else {
                window.location.href = "/orderstatus/" + response.SaleID;
            }
        }
    }




    const DeleteShipment = async (e) => {
        const id = e.target.id;
        const data = await fetch(`${process.env.REACT_APP_URL_API}/api/shipment`, {
            method: "DELETE",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer ' + cookies.access_token,
                'Accept': 'application/json',

            },
            body: JSON.stringify({
                id
            }),
        });
        const Address = await data.json();
    };



    const changePayment = async (e) => {
        setPayment(e.target.value);
    };



    return (
        <div className="col-lg-6 col-sm-12 col-xs-12">
            <div className="checkout-details">
                <div className="order-box">
                    <div className="title-box">
                        <div>Productos <span>Total</span></div>
                    </div>
                    <ul className="qty">

                        {
                            props.products ?
                                props.products.map((ad) => (
                                    <li>{ad.product.name} × {ad.qty}  <span>{ad.qty * ad.product.price}</span></li>
                                )) : null
                        }

                    </ul>
                    <ul className="sub-total">
                        <li>Subtotal <span className="count"><GetTotalPrice
                            products={props.products} /></span></li>
                        <li>Envio
                  <div className="shipping">
                                {/* <div className="shopping-option">
                          <input type="checkbox" name="free-shipping" id="free-shipping"/>
                          <label for="free-shipping">Free Shipping</label>
                      </div> */}
                                <div className="shopping-option">
                                    <input type="checkbox" name="local-pickup" id="local-pickup" onChange={(e) => {
                                        if (e.target.checked) { setPickUp(1) } else {
                                            setPickUp(0);
                                        }; console.log(pickup)
                                    }} />
                                    <label for="local-pickup">Recoger en tienda</label>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul className="total">

                        <li>Descuentos
            {
                                props.coupons.map((des) => (
                                    <span className="count">{des.discount}%</span>
                                ))}
                        </li>
                    </ul>
                    <ul className="total">

                        <li>Total <span className="count"><GetDesc
                            products={props.products}
                            coupons={props.coupons} /></span></li>
                    </ul>
                </div>
                <div className="payment-box">
                    <div className="upper-box">
                        <div className="payment-options">
                            <ul>
                                <li>
                                    <div className="radio-option">
                                        <input checked={payment == 2} onChange={event => { changePayment(event) }} type="radio" name="payment-group" id="payment-1" value={2} />
                                        <label htmlFor="payment-1">Transferencia<span
                                            className="small-text">.</span></label>
                                        {
                                            payment == 2 ?
                                                <DepositInfo />
                                            : null
                                        }
                                    </div>
                                </li>
                                <li>
                                    <div className="radio-option">
                                        <input checked={payment == 1} onChange={event => { changePayment(event) }} type="radio" value={1} name="payment-group" id="payment-3" />
                                        <label htmlFor="payment-3">Flow - webpay<span className="image"><img
                                            src={`${process.env.REACT_APP_URL_API}` + '/image/flow.jpg'}
                                            alt="" /></span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="text-right"><a href="#" className="btn-solid btn" onClick={(e) => PaymentProducts(e)}>Terminar y pagar</a></div>
                </div>
            </div>
        </div>
    )
}

export default CheckoutDetail;

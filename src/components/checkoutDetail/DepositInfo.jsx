import React from 'react'
import {useSelector} from 'react-redux';

const DepositInfo = () => {

    const configGlobal = useSelector(store => store.config);

    return (
        <div className="deposit-box">
            <ul className="items">
                <li><b>Banco:</b>&nbsp;{configGlobal.one.deposit_bank}</li>
                <li><b>Tipo de cuenta:</b>&nbsp;{configGlobal.one.deposit_account_type}</li>
                <li><b>Numero de cuenta:</b>&nbsp;{configGlobal.one.deposit_number}</li>
                <li><b>Rut:</b>&nbsp;{configGlobal.one.deposit_rut}</li>
                <li><b>Nombre titular:</b>&nbsp;{configGlobal.one.deposit_name}</li>
            </ul>
        </div>
    )
}

export default DepositInfo

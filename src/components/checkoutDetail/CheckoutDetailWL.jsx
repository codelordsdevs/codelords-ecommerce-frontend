import React from 'react';
import GetTotalPriceWL from "../getTotalPrice/GetTotalPriceWL";
import GetDesc from "../getTotalPrice/GetDesc";
import { useCookies } from "react-cookie";
import {deleteAllProductsLS} from "../../localStorage/cartLS";
import {setValidations} from "../../redux/paymentDucks";
import {useDispatch} from 'react-redux';
import DepositInfo from './DepositInfo';

const CheckoutDetailWL = (props) => {
    const dispatch = useDispatch()
    const [cookies, setCookie] = useCookies(["user"]);
   	const [pickup,setPickUp] = React.useState(0);
	const [payment,setPayment] = React.useState(2);

    const [cart, setCart] = React.useState([])

	React.useEffect(() =>{
		getCartProducts()
	}, [])

	const PaymentProducts = async (e) => {
        e.preventDefault();
        let validacion = {
            'firstName': true,
            'lastName': true,
            'email': true,
            'phone': true,
            'rut': true,
            'region': true,
            'comuna': true,
            'address': true
        };
        let contador = 0;

        if(props.paymentDetail.FirstName.length == 0){
            validacion.firstName = false;
            contador++;
        }

        if(props.paymentDetail.LastName.length == 0){
            validacion.lastName = false;
            contador++;
        }

        if(!validaTelefono(props.paymentDetail.Phone)){
            validacion.phone = false;
            contador++;
        }

        if(!validaEmail(props.paymentDetail.Email)){
            validacion.email = false;
            contador++;
        }

        if(!validaRut(props.paymentDetail.Rut)){
            validacion.rut = false;
            contador++;
        }

        if(props.paymentDetail.Region == 0){
            validacion.region = false;
            contador++;
        }

        if(props.paymentDetail.Address.length == 0){
            validacion.address = false;
            contador++;
        }
        dispatch(setValidations(validacion));
        if(contador > 0){
            return
        }

		const payment_id = payment;
		const sale_state_id = 2;
	   	// const postal_code = props.postal_code;
		const address = props.paymentDetail.Address;
	  	const receiver_name = props.paymentDetail.FirstName + ' ' + props.paymentDetail.LastName;
		const phone = props.paymentDetail.Phone;
	 	const email = props.paymentDetail.Email;
		const city_id = props.paymentDetail.Comuna;
	 	// const save_shipment = props.save_shipment;
		// const name = props.shipment_name;
        const products = JSON.stringify(cart.data);

    	const data = await fetch(`${process.env.REACT_APP_URL_API}/api/sale`,{
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : 'Bearer '+cookies.access_token,
                'Accept' : 'application/json',
            },	
            body: JSON.stringify({
                payment_id,
                // postal_code,
                address,
                receiver_name,
                phone,
                email,
                city_id,
                pickup,
                // save_shipment,
                products
			}),
		});

		const response = await data.json();
		if (response.status == "true") {
            deleteAllProductsLS();
            if (!response.SaleID > 0) {
                window.location.href = response.url;
            } else {
                window.location.href = "/orderstatus/" + response.SaleID;
            }
        }
	}

    const DeleteShipment = async (e) => {
            const id = e.target.id;
            const data = await fetch(`${process.env.REACT_APP_URL_API}/api/shipment`, {
            method: "DELETE",
            mode: "cors",
            cache: "no-cache",
            headers: {
            "Content-Type": "application/json",
            'Authorization' : 'Bearer '+cookies.access_token,
            'Accept' : 'application/json',

            },
            body: JSON.stringify({
            id
            }),
        });
        const Address = await data.json();
    };

    const changePayment = async (e) => {
        setPayment(e.target.value);
    };

    const getCartProducts = () => {
        let cartStorage = JSON.parse(localStorage.getItem('cart'))
        let newCart = []
        newCart['data'] = cartStorage
        setCart(newCart)
        console.log(cart)
    }

    const validarOferta = (isOffer) => {
        if((isOffer.invert == 0) && (isOffer.y > 0 || isOffer.m > 0 || isOffer.d > 0 || isOffer.h > 0 || isOffer.i > 0)){
            return 1
        }
        return 0
	}

    const numberWithPoints = (number) => {
		return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
	}

    const validaRut = (rutCompleto) => {
		rutCompleto.replaceAll('.','');
		if(rutCompleto.indexOf('-') == -1){
			let rutSinDv = rutCompleto.substring(0, rutCompleto.length - 1);
			let digitoVerificador = rutCompleto.substr(rutCompleto.length - 1);
			let newRut = rutSinDv + '-' + digitoVerificador;
			rutCompleto = newRut;
			//setRut(newRut);
		}
		if (!/^[0-9]+-[0-9kK]{1}$/.test(rutCompleto))
            return false;
        var tmp = rutCompleto.split('-');
        var digv = tmp[1];
        var rut = tmp[0];
        if (digv == 'K') digv = 'k';
        return (dv(rut) == digv);
	}

	const dv = (T) => {
		var M = 0,
            S = 1;
        for (; T; T = Math.floor(T / 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        return S ? S - 1 : 'k';
	}

	const validaTelefono = (telefono) => {
		if (!/^[\+]?[(]?[0-9]{1,3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(telefono)) {
			return false
		}
		return true
	}

	const validaEmail = (email) => {
		if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email)) {
			return false
		}
		return true
	}

    return  (
    <div className="col-lg-6 col-sm-12 col-xs-12">
        <div className="checkout-details">
            <div className="order-box">
                <div className="title-box">
                    <div>Productos <span>Total</span></div>
                </div>
                <ul className="qty">

                    {
                    cart && cart.data && cart.data.length > 0 ?
                    cart.data.map((ad) => (
                        ad.selected ?
                            <li>{ad.product.name} × {ad.qty}  <span>${validarOferta(ad.product.isOffer) ? numberWithPoints(ad.qty * ad.product.special_price) : numberWithPoints(ad.qty * ad.product.price)}</span></li>
                        : null
                    )):null
                    }

                </ul>
                <ul className="sub-total">
                    <li>Subtotal <span className="count">
                        <GetTotalPriceWL products={cart.data}/></span></li>
                    <li>Envio
                        <div className="shipping">
                            {/* <div className="shopping-option">
                                <input type="checkbox" name="free-shipping" id="free-shipping"/>
                                <label for="free-shipping">Free Shipping</label>
                            </div> */}
                            <div className="shopping-option">
                                <input type="checkbox" name="local-pickup" id="local-pickup" onChange={(e)=>{
                                    if(e.target.checked){setPickUp(1)}else{
                                        setPickUp(0);
                                    };console.log(pickup)}}/>
                                <label for="local-pickup">Recoger en tienda</label>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul className="total">
                
                <li>Descuentos
                    {/* {
                    props.coupons.map((des)=>(            
                    <span className="count">{des.discount}%</span>
                    ))} */}
                    </li>
                </ul>
                <ul className="total">
                
                    {/* <li>Total <span className="count"><GetDesc
                            products={props.products}
                            coupons={props.coupons}/></span></li> */}
                </ul>
            </div>
            <div className="payment-box">
                <div className="upper-box">
                    <div className="payment-options">
                        <ul>
                            <li>
                                <div className="radio-option">
                                    <input checked={payment == 2}  onChange={event => {changePayment(event)}} type="radio" name="payment-group" id="payment-1" value={2}/>
                                    <label htmlFor="payment-1">Transferencia<span
                                        className="small-text">.</span>
                                    </label>
                                    {
                                        payment == 2 ?
                                            <DepositInfo />
                                        : null
                                    }
                                </div>
                            </li>
                            <li>
                                <div className="radio-option">
                                    <input checked={payment == 1}  onChange={event => {changePayment(event)}}  type="radio" value={1} name="payment-group" id="payment-3" />
                                    <label htmlFor="payment-3">Flow - webpay<span className="image"><img
                                        src={`${process.env.REACT_APP_URL_API}`+'/image/flow.jpg'}
                                        alt=""/></span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="text-right"><a href="#" className="btn-solid btn" onClick={(e)=>PaymentProducts(e)}>Terminar y pagar</a></div>
            </div>
        </div>
    </div>
    )
}

export default CheckoutDetailWL

import React from "react";
import { useCookies } from "react-cookie";
import CheckoutDetail from "../checkoutDetail/CheckoutDetail";



const Shipment = (props) => {

  const [SaveShipment, setSaveShipment] = React.useState(0)
  const [ShipmentName, setShipmentName] = React.useState("")

  const [FirstName, setFirstName] = React.useState("");
  const [LastName, setLastName] = React.useState("");
  const [Phone, setPhone] = React.useState("");
  const [Email, setEmail] = React.useState("");
  const [Address, setAddress] = React.useState("");
  const [PostalCode, setPostalCode] = React.useState("");

  const [Regiones, setRegiones] = React.useState([]);
  const [Region, setRegion] = React.useState(0);
  const [RegionName, setRegionName] = React.useState("");

  const [Comunas, setComunas] = React.useState([]);
  const [Comuna, setComuna] = React.useState("");
  const [ComunaName, setComunaName] = React.useState("");

  const [shipments, setShipments] = React.useState([]); //Captura todas las direcciones
  const [shipment, setShipment] = React.useState("0"); //ID de shipment en la bdd

  const [cookies, setCookie] = useCookies(["user"]);

  const getShipment = async () => {
    const data = await fetch(`${process.env.REACT_APP_URL_API}/api/shipment`, {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + cookies.access_token,
        'Accept': 'application/json',
      },
    });
    const Address = await data.json();
    setShipments(Address.data);
    console.log("Shipments", shipments);
  };
  const DeleteShipment = async (e) => {
    const id = e.target.id;
    const data = await fetch(`${process.env.REACT_APP_URL_API}/api/shipment`, {
      method: "DELETE",
      mode: "cors",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + cookies.access_token,
        'Accept': 'application/json',

      },
      body: JSON.stringify({
        id
      }),
    });
    const Address = await data.json();
    getShipment();
  };
  React.useEffect(() => {
    GetRegiones()
    getShipment()
  },
    []
  );
  const GetRegiones = async () => {
    const data = await fetch(`${process.env.REACT_APP_URL_API}/api/region`, {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + cookies.access_token,
        'Accept': 'application/json',
      },
    })
    const response = await data.json();
    setRegiones(response);
    console.log("Regiones:", Regiones.data);
  }
  const GetComunas = async (e) => {
    let region_id = e.target.options[e.target.options.selectedIndex].value;
    setRegion(region_id);
    const data = await fetch(`${process.env.REACT_APP_URL_API}/api/city?region_id=` + region_id, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + cookies.access_token,
        'Accept': 'application/json',
      },
    })
    const response = await data.json();
    setComunas(response);
    console.log("Comunas:", Comunas.data);
  }

  const SetShipmentValues = async (e) => {
    //ID SHIPMENT
    let id_shipment = e.target.options[e.target.options.selectedIndex].value;
    console.log("ShipmentValue:", id_shipment)
    setShipment(id_shipment)

    let id;
    if (id_shipment != "0") {
      for (let k = 0; k < shipments.length; k++) {
        if (shipments[k].id == id_shipment) {
          id = shipments[k].city_id;
        }
      }
      for (let i = 0; i < shipments.length; i++) {
        if (shipments[i].id == id_shipment) {
          setAddress(shipments[i].address)
          const names_first_last = shipments[i].receiver_name.split(" ")
          setFirstName(names_first_last[0])
          setLastName(names_first_last[1])
          setPhone(shipments[i].phone)
          setEmail(shipments[i].email)
          setPostalCode(shipments[i].postal_code)
          setShipmentName(shipments[i].name)
          const data = await fetch(`${process.env.REACT_APP_URL_API}/api/city`, {
            method: 'POST',
            mode: 'cors',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + cookies.access_token,
              'Accept': 'application/json',
            },
            body: JSON.stringify({
              id
            }),
          });
          const response = await data.json();
          const region_id = response.data[0].region_id;
          setComunaName(response.data[0].name);
          setRegion(region_id);
          setComuna(id);
          const data_region = await fetch(`${process.env.REACT_APP_URL_API}/api/region`, {
            method: 'POST',
            mode: 'cors',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + cookies.access_token,
              'Accept': 'application/json',
            },
            body: JSON.stringify({
              region_id,
            }),
          });
          const response_region = await data_region.json();
          setRegionName(response_region.data[0].name);
        }
      }
    }

  }



  return (

    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">

        <div class="checkout-title">
          <h3>Detalles de facturación</h3>
        </div>
        <div class="row check-out">
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <div class="field-label">Nombre</div>
            <input type="text" name="field-name" value={FirstName} placeholder="" onChange={(e) => setFirstName(e.target.value)} />
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <div class="field-label">Apellido</div>
            <input type="text" name="field-name" value={LastName} placeholder="" onChange={(e) => setLastName(e.target.value)} />
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <div class="field-label">Telefono</div>
            <input type="text" name="field-name" value={Phone} placeholder="" onChange={(e) => setPhone(e.target.value)} />
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <div class="field-label">Email</div>
            <input type="text" name="field-name" value={Email} placeholder="" onChange={(e) => setEmail(e.target.value)} />
          </div>
          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="field-label">Direcciones Guardadas</div>
            <select onChange={(e) => SetShipmentValues(e)}>
              {
                shipments ?
                  shipments.map((ad) => (
                    <option value={ad.id}>{ad.name}</option>
                  )) : null
              }
              <option value="0">Nueva direccion</option>
            </select>
          </div>
          {
            shipment == "0" ?

              <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <div class="field-label">Nombre Direccion</div>
                <input type="text" name="field-name" onChange={e => setShipmentName(e.target.value)} placeholder="" />
              </div>

              : null
          }
          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="field-label">Region</div>

            {
              Regiones.data ?
                shipment == "0" ?
                  <select value={Region} onChange={(e) => GetComunas(e)}>
                    {
                      Regiones.data.map((ad) => (<option value={ad.id}>{ad.name}</option>))
                    }
                  </select>
                  :
                  <input type="text" name="field-name" value={RegionName} placeholder="Street address" />
                : null
            }

          </div>
          {
            shipment == "0" ?
              Comunas.data ?
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="field-label">Comuna</div>
                  <select onChange={(e) => setComuna(e.target.options[e.target.options.selectedIndex].value)}>
                    {
                      Comunas.data.map((ad) => (<option value={ad.id}>{ad.name}</option>))
                    }
                  </select>
                </div>
                : null
              :
              <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="field-label">Comuna</div>
                <input type="text" name="field-name" value={ComunaName} placeholder="Street address" />
              </div>
          }
          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="field-label">Direccion</div>
            <input type="text" name="field-name" value={Address} placeholder="Street address" onChange={(e) => setAddress(e.target.value)} />
          </div>
          <div class="form-group col-md-12 col-sm-6 col-xs-12">
            <div class="field-label">Codigo Postal</div>
            <input type="text" name="field-name" value={PostalCode} placeholder="" onChange={(e) => setPostalCode(e.target.value)} />
          </div>
          {
            shipment == "0" ?

              <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input onChange={(e) => {
                  if (e.target.checked) { setSaveShipment(1) } else {
                    setSaveShipment(0);
                  }; console.log(SaveShipment)
                }}
                  type="checkbox" name="shipping-option" id="account-option" /> &ensp;
          <label for="account-option" >¿Guardar direccion?</label>
              </div>
              : null
          }

        </div>


      </div>
      <CheckoutDetail
        products={props.products}
        coupons={props.coupons}
        address={Address}
        receiver_name={FirstName + ' ' + LastName}
        phone={Phone}
        email={Email}
        city_id={Comuna}
        postal_code={PostalCode}
        save_shipment={SaveShipment}
        shipment_name={ShipmentName}

      />
    </div>



  )
}
export default Shipment;

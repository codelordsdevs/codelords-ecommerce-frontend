import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { Helmet } from 'react-helmet'

import { Link } from "react-router-dom"
import { useDispatch, useSelector } from 'react-redux'
import { getConfig } from '../../redux/configDucks'
import { searchProduct, getAllProductsAction } from '../../redux/productDucks'
import { useLocation, useHistory } from 'react-router-dom'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { useCookies } from "react-cookie"

const Header = forwardRef((props, ref) => {

    const dispatch = useDispatch()

    const configGlobal = useSelector(store => store.config)

    if(configGlobal.one){
        document.documentElement.style.setProperty('--theme-deafult', configGlobal.one.primary_color)
    }

    const [modal, setModal] = useState(false)

    const [cookies, setCookie] = useCookies(["user"])

    const toggle = () => setModal(!modal)

    const [cart, setCart] = React.useState([])

    const openModal = () => {
        getCartProducts()
        setModal(!modal)
    }

    React.useEffect(() =>{
		getCartProducts()
	}, [])

	const getCartProducts = async () => {
		if(cookies.access_token != null){
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/cart`, {
				method: 'GET',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json',
					'Authorization' : 'Bearer '+cookies.access_token,
					'Accept' : 'application/json',
				}
			});
			const db_cart = await data.json();
			setCart(db_cart);
		} else {
			let cartStorage = JSON.parse(localStorage.getItem('cart'))
			let newCart = []
			newCart['data'] = cartStorage
			setCart(newCart)
		}
	}

    useImperativeHandle(
        ref,
        () => ({
            getInitialConfig() {
                dispatch(getConfig())
            }
        })
    )

    const [buscar, setBuscar] = React.useState('')

    const changeBuscar = (e) => {
        setBuscar(e.target.value)
    }

    const location = useLocation()
    const history = useHistory()

    const buscarProductos = (e) => {
        e.preventDefault()
        if(buscar !== '') {
            if(location.pathname === '/productlist'){
                let value = buscar
                setBuscar('')
                dispatch(searchProduct(value))
            } else {
                let value = buscar
                setBuscar('')
                dispatch(searchProduct(value))
                history.push('/productlist')
            }
        }        
    }

    const redirectShopingCart = () => {
        toggle()
        history.push('/cart')
    }

    const redirectMyAccount = () => {
        history.push('/login')
    }

    return (
        <header>
            <Helmet>
                <title>{ configGlobal.one.name }</title>
            </Helmet>
            <div className="top-header d-none d-sm-block">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="header-contact">
                                <ul>
                                    <li>Bienvenido a { configGlobal.one.name }</li>
                                    <li><i className="fa fa-phone" aria-hidden="true"></i>Llámanos: { configGlobal.one.phone_number }</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-6 text-right">
                            <div className="myAccount">
                                <div className="titleAccount" onClick={redirectMyAccount}>
                                    <i className="fa fa-user" aria-hidden="true"></i> Mi Cuenta
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="main-menu">
                            <div className="menu-left">
                                {/* <div className="navbar">
                                    <a href="javascript:void(0)">
                                        <div className="bar-style"><i className="fa fa-bars sidebar-bar" aria-hidden="true"></i>
                                        </div>
                                    </a>
                                </div> */}
                                <div className="brand-logo">
                                    <a href="/"><img width="150" src={ `${process.env.REACT_APP_URL_API}/image/logo_web.png` } className="img-fluid blur-up lazyload" alt=""/></a>
                                </div>
                            </div>
                            <div className="menu-right pull-right">
                                <div>
                                    <nav>
                                        <div className="toggle-nav"><i className="fa fa-bars sidebar-bar"></i></div>
                                        <ul className="sm pixelstrap sm-horizontal mt-1">
                                            <li>
                                                <div className="mobile-back text-right">Back<i className="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                                            </li>
                                            <li>
                                                <Link to="/" >Inicio</Link>
                                            </li>
                                            <li>
                                                <Link to="/productlist" >Productos</Link>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div>
                                    <div className="icon-nav d-none d-sm-block">
                                        <ul>
                                            <li className="onhover-div mobile-search">
                                                <form className="form-inline" onSubmit={buscarProductos}>
                                                    <input className="mr-sm-2 search-input" type="search" placeholder="Buscar" aria-label="Search" onChange={changeBuscar} value={buscar}></input>
                                                    <div onClick={buscarProductos}>
                                                        <img src="../assets/images/icon/search.png" className="img-fluid blur-up lazyload" alt="" /> <i className="ti-search" ></i>
                                                    </div>
                                                </form>
                                            </li>
                                            {/* <li className="onhover-div mobile-setting">
                                                <div><img src="../assets/images/icon/setting.png" className="img-fluid blur-up lazyload" alt="" /> <i className="ti-settings"></i></div>
                                            </li> */}
                                            <li className="onhover-div mobile-cart dropdown">
                                                <div onClick={openModal}><img src="../assets/images/icon/cart.png" className="img-fluid blur-up lazyload" alt="" /> <i className="ti-shopping-cart"></i></div>
                                            </li>
                                            <Modal isOpen={modal} toggle={toggle} >
                                                <ModalHeader toggle={toggle}>Carrito de compras</ModalHeader>
                                                <ModalBody>
                                                    <div className="container">
                                                        {
                                                            cart.data && cart && cart.data.length > 0 ?
                                                                cart.data.map((item, index) => (
                                                                    index < 3 ?
                                                                    <div className="row border rounded mt-1">
                                                                        <div className="col-4">
                                                                            <img 
                                                                                src={item.product.product_images != undefined && item.product.product_images.length > 0 ? item.product.product_images[0].url : null} 
                                                                                height='150px'>
                                                                            </img>
                                                                        </div>
                                                                        <div className='col-4 mt-5'>
                                                                            <h4>{item.product.name}</h4>
                                                                            <h4>{item.product.isOffer != undefined && item.product.isOffer.invert == 0 && item.product.isOffer.days > 0 ? '$' + item.product.formated_special_price : '$' + item.product.formated_price}</h4>
                                                                        </div>
                                                                        <div className='col-4 mt-5'>
                                                                            <h4>Cantidad</h4>
                                                                            <h4>{item.qty}</h4>
                                                                        </div>
                                                                    </div>
                                                                    : null
                                                                ))
                                                            : <div>Tu carrito esta vacío</div>
                                                        }
                                                    </div>
                                                </ModalBody>
                                                <ModalFooter>
                                                    <div className="col-8" onClick={redirectShopingCart}><a href="#" className="btn btn-solid rounded">Ver carrito de compras {cart.data && cart && cart.data.length > 3 ? '(' + cart.data.length + ')' : null}</a></div>
                                                </ModalFooter>
                                            </Modal>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
})

export default Header

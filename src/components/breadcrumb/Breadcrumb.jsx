import React from 'react';

const Breadcrumb = (props) => {
	return (
		<div className="breadcrumb-section">
        		<div className="container">
            			<div className="row">
                			<div className="col-sm-6">
                    				<div className="page-title">
                        				<h2>{props.title}</h2>
                    				</div>
                			</div>
                			<div className="col-sm-6">
                    				<nav aria-label="breadcrumb" className="theme-breadcrumb">
                        				<ol className="breadcrumb">
								{
									props.breadcrumbs ?
									props.breadcrumbs.length > 0 ? props.breadcrumbs.map(breadcrumb => (
											breadcrumb.active == "0" ? 
											<li key={breadcrumb.id} className="breadcrumb-item"><a href={breadcrumb.url}>{breadcrumb.title}</a></li>
											: 
											<li key={breadcrumb.id} className="breadcrumb-item active" aria-current="page">{breadcrumb.title}</li>
										)) :
										null
									:
									null
								}
                        				</ol>
                    				</nav>
                			</div>
            			</div>
        		</div>
    		</div>	
	);
}

export default Breadcrumb;

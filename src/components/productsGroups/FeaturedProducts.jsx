import React from 'react'
import ProductBox from '../products/ProductBox'

import {useDispatch} from 'react-redux'
import {setLoading} from '../../redux/loadingDucks'

const FeaturedProducts = (props) => {

    const dispatch = useDispatch()

    const [featured, setfeatured] = React.useState([])
    const [offers, setOffers] = React.useState([])
    const [news, setNews] = React.useState([])
    const [specialBox, setSpecialBox] = React.useState('novedades')

    React.useEffect(() =>{
        getOptionsFeatured()
        getOptionsDiscount()
        getOptionsNews()
    }, [])

    const getOptionsFeatured = async () => {
        dispatch(setLoading(1))
        try {
            const data = await fetch(`${process.env.REACT_APP_URL_API}/api/product?featured=1&limit=8`, {
                method: 'OPTIONS',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            const prods = await data.json()
            setfeatured(prods)
            dispatch(setLoading(-1))
        } catch(error){
            dispatch(setLoading(-1))
        }
    }

    const getOptionsDiscount = async () => {
        dispatch(setLoading(1))
        try {
            const data = await fetch(`${process.env.REACT_APP_URL_API}/api/product?discount=1&limit=8`, {
                method: 'OPTIONS',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            const prods = await data.json()
            setOffers(prods)
            dispatch(setLoading(-1))
        } catch(error){
            dispatch(setLoading(-1))
        }
    }

    const getOptionsNews = async () => {
        dispatch(setLoading(1))
        try {
            const data = await fetch(`${process.env.REACT_APP_URL_API}/api/product?news=1&limit=8`, {
                method: 'OPTIONS',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            const prods = await data.json()
            setNews(prods)
            dispatch(setLoading(-1))
        } catch(error){
            dispatch(setLoading(-1))
        }
    }

    const changeType = (e, type) => {
        e.preventDefault()
        setSpecialBox(type)
    }

    return (
        <div>
            <div className="title1 section-t-space">
                {/* <h4>exclusive products</h4> */}
                <h2 className="title-inner1">{props.title}</h2>
            </div>
            <section className="section-b-space p-t-0 ratio_asos">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="theme-tab">
                                <ul className="tabs tab-title">
                                    {
                                        news.data && news.data.length > 0?
                                        <li className={ specialBox == 'novedades' ? 'current' : ''}><a href="" onClick={ e => changeType(e,'novedades')}>Novedades</a></li>
                                        : null
                                    }
                                    {
                                        featured.data && featured.data.length > 0 ?
                                            <li className={ specialBox == 'destacados' ? 'current' : ''}><a href="" onClick={ e => changeType(e,'destacados')}>Destacados</a></li>
                                        : null
                                    }
                                    {
                                        offers.data && offers.data.length > 0 ?
                                            <li className={ specialBox == 'ofertas' ? 'current' : ''}><a href="" onClick={ e => changeType(e,'ofertas')}>Ofertas</a></li>
                                        : null
                                    }
                                </ul>
                                {
                                    specialBox == 'novedades' ?
                                        <div className="tab-content-cls">
                                            <div id="tab-4" className="tab-content active default" style={{display: "block"}}>
                                                <div className="no-slider row">
                                                {
                                                    news.data ? 
                                                    news.data.length > 0 ? news.data.map(news => (
                                                        <ProductBox 
                                                            key={news.id}
                                                            id={news.id} 
                                                            name={news.name} 
                                                            price={news.price} 
                                                            stock={news.stock} 
                                                            description={news.description} 
                                                            featured={news.featured} 
                                                            category_id={news.category_id} 
                                                            special_price={news.special_price} 
                                                            special_price_end_date={news.special_price_end_date}
                                                            product_images={news.product_images}
                                                            isOffer={news.isOffer}
                                                            formated_price={news.formated_price}
                                                            formated_special_price={news.formated_special_price}
                                                        />
                                                        )) :
                                                        null
                                                    :
                                                    null
                                                }
                                                </div>
                                            </div>
                                        </div> :
                                    null
                                }
                                {
                                    specialBox == 'destacados' ?
                                        <div className="tab-content-cls">
                                            <div id="tab-4" className="tab-content active default" style={{display: "block"}}>
                                                <div className="no-slider row">
                                                {
                                                    featured.data ? 
                                                    featured.data.length > 0 ? featured.data.map(featured => (
                                                        <ProductBox 
                                                            key={featured.id}
                                                            id={featured.id} 
                                                            name={featured.name} 
                                                            price={featured.price} 
                                                            stock={featured.stock} 
                                                            description={featured.description} 
                                                            featured={featured.featured} 
                                                            category_id={featured.category_id} 
                                                            special_price={featured.special_price} 
                                                            special_price_end_date={featured.special_price_end_date}
                                                            product_images={featured.product_images}
                                                            isOffer={featured.isOffer}
                                                            formated_price={featured.formated_price}
                                                            formated_special_price={featured.formated_special_price}
                                                        />
                                                        )) :
                                                        null
                                                    :
                                                    null
                                                }
                                                </div>
                                            </div>
                                        </div> :
                                    null
                                }
                                {
                                    specialBox == 'ofertas' ?
                                        <div className="tab-content-cls">
                                            <div id="tab-4" className="tab-content active default" style={{display: "block"}}>
                                                <div className="no-slider row">
                                                {
                                                    offers.data ? 
                                                    offers.data.length > 0 ? offers.data.map(offers => (
                                                        <ProductBox 
                                                            key={offers.id}
                                                            id={offers.id} 
                                                            name={offers.name} 
                                                            price={offers.price} 
                                                            stock={offers.stock} 
                                                            description={offers.description} 
                                                            featured={offers.featured} 
                                                            category_id={offers.category_id} 
                                                            special_price={offers.special_price} 
                                                            special_price_end_date={offers.special_price_end_date}
                                                            product_images={offers.product_images}
                                                            isOffer={offers.isOffer}
                                                            formated_price={offers.formated_price}
                                                            formated_special_price={offers.formated_special_price}
                                                        />
                                                        )) :
                                                        null
                                                    :
                                                    null
                                                }
                                                </div>
                                            </div>
                                        </div> :
                                    null
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        // <div className="album py-5 bg-light">
        //     <div className="container">
        //         <div className="row">
        //         {
        //             products.data ? 
        //                 products.data.length > 0 ? products.data.map(product => (
        //                 <ProductBox 
        //                     key={product.id}
        //                     id={product.id} 
        //                     name={product.name} 
        //                     price={product.price} 
        //                     stock={product.stock} 
        //                     description={product.description} 
        //                     featured={product.featured} 
        //                     category_id={product.category_id} 
        //                     special_price={product.special_price} 
        //                     special_price_end_date={product.special_price_end_date} 
        //                 />
        //                 )) :
        //                 <h1>no hay productos</h1>
        //             :
        //             <h1>no hay productos</h1>
        //         }
        //         </div>
        //     </div>
        // </div>
    )
}

export default FeaturedProducts

import React, { useState, useRef, useContext } from "react"
import { useParams } from "react-router-dom"
import { useCookies } from "react-cookie"
import Slider from 'react-slick'
import { Container, Row, Col, Media } from 'reactstrap'
import {useDispatch} from 'react-redux'
import {setLoading} from '../../redux/loadingDucks'
import {addProductInCartAction, getHtmlToast} from '../../redux/cartDucks'
import { toast } from 'react-toastify';
import {addProductLS} from '../../localStorage/cartLS'
import { SketchPicker, CirclePicker } from 'react-color'

const ProductDetail = () => {

	const dispatch = useDispatch()

	const [product, setProduct] = React.useState([]);
	const [addedProd, setAddedProd] = React.useState();
	const [qty, setQty] = React.useState(1);
	const [cookies, setCookie] = useCookies(["user"]);
	const [subProducts, setSubProducts] = React.useState([]);
	const [colors, setColors] = React.useState([]);
	const [selectedProduct, setSelectedProduct] = React.useState({});
	const [colorCodes, setColorCodes] = React.useState({});
	const [finalColor, changeColor] = React.useState("#fff");

  	const { id } = useParams();

	const [nav1, setNav1] = useState();
  	const [nav2, setNav2] = useState();

	var products = {
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		fade: true
	};
	var productsnav = {
		slidesToShow: 4,
		swipeToSlide: true,
		arrows: false,
		focusOnSelect: true,
		infinite: false
	};

  	React.useEffect(() => {
		getProduct();
	},[]);

	const filterClick = () => {
		document.getElementById("filter").style.left = "-15px";
	} 

	const getProduct = async () => {
		dispatch(setLoading(1))
		try{
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/product?id=` + id, {
				method: "POST",
				mode: "cors",
				cache: "no-cache",
				headers: {
					"Content-Type": "application/json",
				},
			});
			const prod = await data.json();
			setProduct(prod);
			getSubProducts(prod);
			if(prod.data[0].subproducts.length == 0){
				setSelectedProduct(prod);
			}
			dispatch(setLoading(-1))
		}catch(e){
			dispatch(setLoading(-1))
		}
	}

	const addProduct = async () => {
		if(cookies.access_token != null){
			let param = id
			if(selectedProduct){
				param = selectedProduct.data[0].id
			}
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/cart?product_id=${id}&qty=${qty}&user_id=1`,{
				method: "PUT",
				mode: "cors",
				cache: "no-cache",
				headers:{
				"Content-Type": "application/json",
				'Authorization' : 'Bearer '+cookies.access_token,
				'Accept' : 'application/json',
				},
				});
				const addProd = await data.json();
				setAddedProd(addProd);
				toast(getHtmlToast(product.data[0]));
		} else {
			let prod = product.data[0]
			console.log(selectedProduct)
			if(selectedProduct.data !== undefined && selectedProduct.data.length > 0){
				prod = selectedProduct.data[0]
			}
			// return
			addProductLS(prod, qty)
			// dispatch(addProductInCartAction(product.data[0]))
		}
	}

	const validarOferta = (isOffer) => {
        if((isOffer.invert == 0) && (isOffer.y > 0 || isOffer.m > 0 || isOffer.d > 0 || isOffer.h > 0 || isOffer.i > 0)){
            return 1
        }
        return 0
	}
	
	const changeQty = (value) => {
		let newQty = qty + value
		setQty(newQty)
	}

	const getSubProducts = (prod) => {
		let producto = prod.data[0]
		let arrayProducts = [];
		arrayProducts.push(producto);
		if(producto.subproducts && producto.subproducts.length > 0){
			for(let x = 0; x < producto.subproducts.length; x++){
				arrayProducts.push(producto.subproducts[x]);
			}
		}
		setSubProducts(arrayProducts);
	}

	const getColors = (prod) => {
		let producto = prod.data[0]
		let arrayProducts = [];
		let arrayOnlyCode = [];
		console.log(producto)
		if(producto.subproducts && producto.subproducts.length > 0){
			for(let x = 0; x < producto.subproducts.length; x++){
				if(producto.subproducts[x].product_type_id == 2){
					arrayProducts.push(producto.subproducts[x]);
					arrayOnlyCode.push(producto.subproducts[x].color_code);
				}
				
			}
		}
		setColors(arrayProducts);
		setColorCodes(arrayOnlyCode);
		console.log(colors)
	}

	const selectProduct = async (e) => {
		if(e.target.value == 0){
			return
		}
		dispatch(setLoading(1))
		try{
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/subproduct?id=` + e.target.value, {
				method: "POST",
				mode: "cors",
				cache: "no-cache",
				headers: {
					"Content-Type": "application/json",
				},
			});
			const prod = await data.json();
			setSelectedProduct(prod);
			getColors(prod);
			dispatch(setLoading(-1))
		}catch(e){
			dispatch(setLoading(-1))
		}
	}

	const selectColor = async (e) => {
		if(e.target.value == 0){
			return
		}
		dispatch(setLoading(1))
		try{
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/subproduct?id=` + e.target.value, {
				method: "POST",
				mode: "cors",
				cache: "no-cache",
				headers: {
					"Content-Type": "application/json",
				},
			});
			const prod = await data.json();
			setSelectedProduct(prod);
			dispatch(setLoading(-1))
		}catch(e){
			dispatch(setLoading(-1))
		}
	}

	const onColorChange = async (color, event) => {
		changeColor(color.hex)

		let colorSelected = colors.filter(item => item.color_code.toUpperCase() == color.hex.toUpperCase())
		dispatch(setLoading(1))
		try{
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/subproduct?id=` + colorSelected[0].id, {
				method: "POST",
				mode: "cors",
				cache: "no-cache",
				headers: {
					"Content-Type": "application/json",
				},
			});
			const prod = await data.json();
			setSelectedProduct(prod);
			dispatch(setLoading(-1))
		}catch(e){
			dispatch(setLoading(-1))
		}
	}

	return product.data ? (
		product.data.length > 0 ? (
		// section start
		<section>
			<div className="collection-wrapper">
			<div className="container">
				<div className="row">
				<div className="col-lg-12 col-sm-12 col-xs-12">
					<div className="container" fluid='true'>
					<div className="row">
						<div className="col-lg-6 product-thumbnail" >
							<Slider {...products} asNavFor={nav2} ref={(slider1) => setNav1(slider1)} className="product-slick">
							{
							product.data[0].product_images.length > 0 ?
								product.data[0].product_images.map((item, index) => 
								<div key={index}>
									<Media src={`${item.url}`} alt={item.name} className="img-fluid image_zoom_cls-0" style={{'margin': 'auto'}}/>
									{/* <div className="media">
									<img className="align-self-start mr-3" src={item.url} alt="Generic placeholder image"></img>
									</div> */}
								</div>
								)
							: null
							}
							{/* {
								product.data[0].subproducts.length > 0 ?
									product.data[0].subproducts.map((item, index) => {
										item.product_images && item.product_images.length > 0 ?
											item.product_images.map((image, imageIndex) => {
												<div key={index}>
													<Media src={`${image.url}`} alt={image.name} className="img-fluid image_zoom_cls-0" style={{'margin': 'auto'}}/>
												</div>
											})
										: null
									})
								: null
							} */}
							</Slider>
							{
								product.data[0].product_images.length >= 0 ?
								<Slider className="slider-nav" {...productsnav}
									asNavFor={nav1}
									ref={(slider2) => setNav2(slider2)}>	
									{
										product.data[0].product_images.map((item, index) => 
										<div key={index}>
											<Media src={`${item.url}`} key={index} alt={item.name} className="img-fluid" />
											{/* <div className="media" >
											<img className="img-fluid" src={item.url} alt="Generic placeholder image"></img>
											</div> */}
										</div>
										)
									}						
									
								</Slider>
								: null
							}
							
						</div>
						<div className="col-lg-6 rtl-text">
						<div className="product-right">
							<h2 className="mb-0">{product.data[0].name}</h2>
							{/*  <h5 className="mb-2">
							by <a href="javascript:void(0);">zara</a>
							</h5> 
						
							<h4>
							<del>$459.00</del>
							<span>55% off</span>
							</h4>
							*/}
							{ 
								validarOferta(product.data[0].isOffer) == 1 ? 
									<h3>${product.data[0].formated_special_price} <del>${product.data[0].formated_price}</del></h3> 
								: <h3>${product.data[0].formated_price}</h3>
							}
							{/* <ul className="color-variant">
							<li className="bg-light0"></li>
							<li className="bg-light1"></li>
							<li className="bg-light2"></li>
							</ul> */}
							<div className="product-description border-product">
							{/* <h6 className="product-title size-text">
								Selecionar talla
								<span>
								<a href="javascript:void(0);" data-toggle="modal" data-target="#sizemodal">
									Tabla de tallas
								</a>
								</span>
							</h6> */}
							{/* <div className="modal fade" id="sizemodal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div className="modal-dialog modal-dialog-centered" role="document">
								<div className="modal-content">
									<div className="modal-header">
									<h5 className="modal-title" id="exampleModalLabel">
										Sheer Straight Kurta
									</h5>
									<button type="button" className="close"   data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>
									<div className="modal-body">
									<img src={size_chart_img} alt="" className="img-fluid blur-up lazyload"></img>
									</div>
								</div>
								</div>
							</div> */}
							{/* <div className="size-box">
								<ul>
								<li className="active">
									<a href="javascript:void(0);">s</a>
								</li>
								<li>
									<a href="javascript:void(0);">m</a>
								</li>
								<li>
									<a href="javascript:void(0);">l</a>
								</li>
								<li>
									<a href="javascript:void(0);">xl</a>
								</li>
								</ul>
							</div> */}
							<h6 className="product-title">Cantidad</h6>
							<div className="qty-box">
								<div className="input-group">
								<span className="input-group-prepend">
								
									<button type="button" className="btn quantity-left-minus"  data-type="minus"data-field="" onClick={() => {if (qty>1) {changeQty(-1)}}} >
									<i className="ti-angle-left"></i>
									</button>

								</span>
								<input type="text" name="quantity" className="form-control input-number" value={qty}></input>
								<span className="input-group-prepend">

									<button type="button"  className="btn quantity-right-plus"  data-type="plus" data-field="" onClick={() => changeQty(1)} >
									<i className="ti-angle-right"></i>
									</button>

								</span>
								</div>
							</div>
			
							<h6 className="product-title size-text">Stock: {product.data[0].stock}</h6>
							<div>
								{
									product.data[0].subproducts && product.data[0].subproducts.length > 0 ?
										<div>
											<div className="form-group">
            									<div className="field-label">Opción</div>
												<select className="selectProduct" onChange={(e) => selectProduct(e)}>
													<option value='0'>Seleccione un producto</option>
													{
														subProducts.map((item) => (<option value={item.id}>{item.name}</option>))
													}
												</select>
											</div>
										</div>
										
									: null
								}
							</div>
							<div>
								{
									colors && colors.length > 0 ?
										<div>
											<div className="form-group">
            									<div className="field-label">Color</div>
												{/* <select onChange={(e) => selectColor(e)}>
													<option value='0'>Seleccione un color</option>
													{
														colors.map((item) => (<option value={item.id} style={{backgroundColor: item.color_code}}>{item.name}</option>))
													}
												</select> */}
											</div>
											<div>
												<CirclePicker color={finalColor} colors={colorCodes} onChange={onColorChange}/>
											</div>
										</div>
										
									: null
								}
							</div>
							</div>
							<div className="product-buttons">
							<a href="" data-toggle="modal"  data-target="#addtocart" className="btn btn-solid"  onClick={() => addProduct()} >
								Añadir al carro
							</a>
							{/* <a href="/cart" className="btn btn-solid">
								Comprar
							</a> */}
							</div>
							<div className="border-product">
							<h6 className="product-title">Detalles del Producto </h6>
							{/* <p>{product.data[0].description}</p> */}
							<div dangerouslySetInnerHTML={{ __html: product.data[0].description }}></div>
							</div>
							{/* <div className="border-product">
							<h6 className="product-title">Compartir</h6>
							<div className="product-icon">
								<ul className="product-social">
								<li>
									<a href="javascript:void(0);">
									<i className="fa fa-facebook"></i>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);">
									<i className="fa fa-google-plus"></i>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);">
									<i className="fa fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);">
									<i className="fa fa-instagram"></i>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);">
									<i className="fa fa-rss"></i>
									</a>
								</li>
								</ul>
								<form className="d-inline-block">
								<button className="wishlist-btn">
									<i className="fa fa-heart"></i>
									<span className="title-font">
									Añadir a la lista de deseos
									</span>
								</button>
								</form>
							</div>
							</div> */}
							{/* <div className="border-product">
							<h6 className="product-title">Tiempo restante</h6>
							<div className="timer">
								<p id="demo">
								<span>
									25 <span className="padding-l">:</span>
									<span className="timer-cal">Dias</span>
								</span>
								<span>
									22 <span className="padding-l">:</span>
									<span className="timer-cal">Hrs</span>
								</span>
								<span>
									13 <span className="padding-l">:</span>
									<span className="timer-cal">Min</span>
								</span>
								<span>
									57 <span className="timer-cal">Sec</span>
								</span>
								</p>
							</div>
							</div> */}
						</div>
						</div>
					</div>
					</div>
				</div>
				

					{/* <div className="col-lg-6">
					<div className="product-slick slick-initialized slick-slider">
					<button className="slick-prev slick-arrow" aria-label="Previous" type="button" >Previous</button>
					<div className="slick-list draggable">
						<div className="slick-track" style={{opacity: '1',width: '2680px'}}>
						<div className="slick-slide" data-slick-index="0" aria-hidden="true" style={{width : '670px',
						position: 'relative', left: '0px', top: '0px', zIndex: '998', opacity: '0',
							transition: 'opacity 500ms ease 0s'}} tabIndex="-1">                       
							<div>
								<div style={{width:'100%',display:'inline-block'}}>
									<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up image_zoom_cls-0 lazyloaded"></img>
								</div>
							</div>
						</div>
						<div className="slick-slide slick-current slick-active" data-slick-index="1" aria-hidden="false"
						style={{width: '670px', position: 'relative', left: '-690px', top: '0px', zIndex: '999', opacity: '1'}}>                  
							<div>
								<div style={{width: '100%', display: 'inline-block'}}>
								<img src={product.data[0].product_images[1].url}  alt="" className="img-fluid blur-up image_zoom_cls-1 lazyloaded"></img>
								</div>                      
							</div>
						</div>
						<div className="slick-slide" data-slick-index="2" aria-hidden="true"
							tabIndex="-1" style={{width: '670px', position: 'relative',
							left: '-1340px', top: '0px',
							zIndex: '998', opacity: '0'}}>                   
							<div>
								<div style={{width: '100%', display: 'inline-block'}}>
								<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up image_zoom_cls-2 lazyloaded"></img>
								</div>                              
							</div> 
						</div>  
						<div className="slick-slide" data-slick-index="3" aria-hidden="true" tabIndex="-1"
							style={{width:'670px', position: 'relative',
							left: '-2010px', top:'0px', zIndex:'998', opacity: '0'}}>
							<div>
								<div style={{width: '100%', display: 'inline-block'}}>
								<img src={product.data[0].product_images[0].url}  alt="" className="img-fluid blur-up image_zoom_cls-3 lazyloaded"></img>
								</div>                       
							</div>  
						</div>                                      
						</div>
					</div>
						<button className="slick-next slick-arrow" aria-label="Next" type="button">Next</button>
					</div>
					<div className="row">
						<div className="col-12 p-0">
							<div className="slider-nav slick-initialized slick-slider">
							<div className="slick-list draggable">
								<div className="slick-track" style={{opacity:'1',width:'2574px',transform:'translate3d(-936px,0px,0px)'}}>
									<div className="slick-slide slide-cloned" data-slick-index="-3" aria-hidden="true"
										tabIndex="-1"
										style={{width:'234px'}}>
									<div>
										<div style={{width:'100%',display:'inline-block'}}>
											<img src={product.data[0].product_images[0].url} alt=""
												className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide slick-cloned" data-slick-index="-2" aria-hidden="true" tabIndex="-1"
										style={{width:'234px'}}>
										<div>
										<div style={{width:'100%', display: 'inline-block'}}>
											<img src={product.data[0].product_images[0].url} alt=""
											className="img-fluid blur-up lazyloaded"></img>
										</div>
										</div>
									</div>
									<div className="slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true"
											tabIndex="-1" style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
											<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide" data-slick-index="0" aria-hidden="true" tabIndex="-1"
											style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
											<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide slick-current slick-active" data-slick-index="1" aria-hidden="false"
											style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
											<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide slick-active" data-slick-index="2" aria-hidden="false"
											style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
										<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide slick-active"  data-slick-index="3" aria-hidden="false"
											style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
											<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide  slick-cloned" data-slick-index="4" aria-hidden="true"
									tabIndex="-1" style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
											<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide slick-cloned" data-slick-index="5" aria-hidden="true"
									tabIndex="-1" style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
										<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide slick-cloned" data-slick-index="6" aria-hidden="true"
									tabIndex="-1" style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
										<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
									<div className="slick-slide slick-cloned" data-slick-index="7" aria-hidden="true"
									tabIndex="-1" style={{width:'234px'}}>
									<div>
										<div style={{width:'100%', display:'inline-block'}}>
										<img src={product.data[0].product_images[0].url} alt="" className="img-fluid blur-up lazyloaded"></img>
										</div>
									</div>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
					</div> */}
				
				</div>
			</div>
			</div>
		</section>
				) : (
		null
		)
	) : (
		null
	);
	};

export default ProductDetail;

import React, { useState } from 'react';
import { useCookies } from "react-cookie";
import {useDispatch} from 'react-redux';
import {setLoading} from '../../redux/loadingDucks';

const AccountInfo = (props) => {

    const dispatch = useDispatch();
    const [inEdit, setInEdit] = useState(false);
    const [changePass, setChangePass] = useState(false);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [oldPass, setOldPass] = useState('');
    const [newPass, setNewPass] = useState('');
    const [confirmPass, setConfirmPass] = useState('');

    const [validFirstName, setValidFirstName] = useState(true);
    const [validLastName, setValidLastName] = useState(true);
    const [validEmail, setValidEmail] = useState(true);
    const [validPhone, setValidPhone] = useState(true);
    const [validOldPass, setValidOldPass] = useState(true);
    const [validNewPass, setValidNewPass] = useState(true);
    const [validConfirmPass, setValidConfirmPass] = useState(true);
    const [validEqualsPass, setValidEqualsPass] = useState(true);

    const [cookies, setCookie] = useCookies(["user"]);

    const editarDatos = (e) => {
        e.preventDefault();
        if(inEdit){
            setInEdit(false);
            setChangePass(false);
        } else {
            setInEdit(true);
        }
    }

    const cambiarPassword = (e) => {
        e.preventDefault();
        if(changePass){
            setChangePass(false);
            setInEdit(false);
        } else {
            setChangePass(true);
        }
    }

    const cancelPass = (e) => {
        e.preventDefault();
        setChangePass(false);
        setInEdit(false);
    }

    const cancelEdit = (e) => {
        e.preventDefault();
        setChangePass(false);
        setInEdit(false);
    }

    const updateUser = async (e) => {
        let contador = 0;
        if(firstName == ''){
            setValidFirstName(false);
            contador++;
        } else {
            setValidFirstName(true);
        }
        if(lastName == ''){
            setValidLastName(false);
            contador++;
        } else {
            setValidLastName(true);
        }
        if(!validaTelefono(phone)){
            setValidPhone(false);
            contador++;
        } else {
            setValidPhone(true);
        }

        if(contador > 0){
            return;
        }

        dispatch(setLoading(1));
        const data = await fetch(`${process.env.REACT_APP_URL_API}/api/user`, {
            method: "PATCH",
            mode: "cors",
            cache: "no-cache",
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + cookies.access_token,
              'Accept': 'application/json',
      
            },
            body: JSON.stringify({
                'id': props.user.id,
                'name': firstName,
                'lastname': lastName,
                // 'email': email,
                'phone': phone
            }),
        });
        const result = await data.json();
        dispatch(setLoading(-1));
        if(result.status == "true"){
            setInEdit(false);
            setChangePass(false);
            setFirstName('');
            setLastName('');
            setPhone('');
            props.callUser();
            alert("Usuario actualizado con exito.");
        }
        console.log(result);
    }

    const changePassword = async (e) => {
        let contador = 0;
        if(oldPass.length < 9){
            setValidOldPass(false);
            contador++;
        } else {
            setValidOldPass(true);
        }
        if(newPass.length < 9){
            setValidNewPass(false);
            contador++;
        } else {
            setValidNewPass(true);
        }
        if(confirmPass.length < 9){
            setValidConfirmPass(false);
            contador++;
        } else {
            setValidConfirmPass(true);
        }
        if(newPass != confirmPass){
            setValidEqualsPass(false);
            contador++;
        } else {
            setValidEqualsPass(true);
        }

        if(contador > 0){
            return;
        }

        dispatch(setLoading(1));
        const data = await fetch(`${process.env.REACT_APP_URL_API}/api/userpassword`, {
            method: "PATCH",
            mode: "cors",
            cache: "no-cache",
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + cookies.access_token,
              'Accept': 'application/json',
      
            },
            body: JSON.stringify({
                'id': props.user.id,
                'oldpassword': oldPass,
                'newpassword': newPass,
                'confirmnewpassword': confirmPass
            }),
        });
        const result = await data.json();
        dispatch(setLoading(-1));
        if(result.status == "true"){
            setInEdit(false);
            setChangePass(false);
            setOldPass('');
            setNewPass('');
            setConfirmPass('');
            props.callUser();
            alert("Contraseña actualizada con exito.");
        } else {
            alert(result.message);
        }
        console.log(result);
    }

    const validaTelefono = (telefono) => {
		if (!/^[\+]?[(]?[0-9]{1,3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(telefono)) {
			return false
		}
		return true
	}

    return (
        <div className="col-lg-9">
            <div className="dashboard-right">
                <div className="dashboard">
                    <div className="box-account box-info">
                        <div className="box-head">
                            <h2>Información de cuenta</h2>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                {
                                    inEdit == false && changePass == false ?
                                        <div className="box">
                                            <div className="box-title">
                                                <h3>Información de usuario</h3>
                                                <a href="#" onClick={editarDatos}>Editar</a>
                                            </div>
                                            {
                                                props.user ? 
                                                <div className="box-content">
                                                    <h6>{props.user.name}</h6>
                                                    <h6>{props.user.email}</h6>
                                                    <h6><a href="#" onClick={cambiarPassword}>Cambiar contraseña</a></h6>
                                                </div>
                                                : null
                                            }
                                        </div>
                                    : 
                                    inEdit == true ?
                                        <div>
                                            <div className="box">
                                                <div className="box-title">
                                                    <h3>Actualizar el usuario</h3>
                                                    <a href="#" onClick={editarDatos}>Editar</a>
                                                </div>
                                                <div className="checkout-page">
                                                    <div className="checkout-form">
                                                        <form>
                                                            <div className="row">
                                                                <div className="col-lg-12 col-sm-12 col-xs-12">
                                                                    <div className="row check-out">
                                                                        <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                                            <div className="field-label">Nombre</div>
                                                                            <input className="form-control is-invalid" type="text" name="field-name" value={firstName} placeholder="" onChange={(e) => setFirstName(e.target.value)} required aria-describedby="inputGroupPrepend3 validationFirstNameFeedback"/>
                                                                            {
                                                                                !validFirstName ?
                                                                                <div id="validationFirstNameFeedback" className="invalid-feedback">
                                                                                    Ingrese un nombre.
                                                                                </div>
                                                                                : null
                                                                            }
                                                                        </div>
                                                                        <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                                            <div className="field-label">Apellido</div>
                                                                            <input className="form-control is-invalid" type="text" name="field-name" value={lastName} placeholder="" onChange={(e) => setLastName(e.target.value)} required aria-describedby="inputGroupPrepend3 validationLastNameFeedback"/>
                                                                            {
                                                                                !validLastName ?
                                                                                <div id="validationLastNameFeedback" className="invalid-feedback">
                                                                                    Ingrese un apellido.
                                                                                </div>
                                                                                : null
                                                                            }
                                                                        </div>
                                                                        <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                                            <div className="field-label">Telefono</div>
                                                                            <input className="form-control is-invalid" type="text" name="field-name" value={phone} placeholder="" onChange={(e) => setPhone(e.target.value)} required aria-describedby="inputGroupPrepend3 validationPhoneNameFeedback"/>
                                                                            {
                                                                                !validPhone ?
                                                                                <div id="validationPhoneFeedback" className="invalid-feedback">
                                                                                    Ingrese un numero de teléfono valido.
                                                                                </div>
                                                                                : null
                                                                            }
                                                                        </div>
                                                                        {/* <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                                            <div className="field-label">Email</div>
                                                                            <input type="text" name="field-name" value={email} placeholder="" onChange={(e) => setEmail(e.target.value)} />
                                                                        </div> */}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <div className="row">
                                                <div className="col-10">
                                                    <div className="payment-box">
                                                        <div className="text-right"><a href="#" className="btn-solid btn" onClick={(e) => cancelEdit(e)}>Cancelar</a></div>
                                                    </div>
                                                </div>
                                                <div className="col-2">
                                                    <div className="payment-box">
                                                        <div className="text-right"><a href="#" className="btn-solid btn" onClick={(e) => updateUser(e)}>Guardar</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    :
                                    changePass == true ?
                                        <div>
                                            <div className="box">
                                                <div className="box-title">
                                                    <h3>Actualizar contraseña</h3>
                                                    <a href="#" onClick={() => setChangePass(false)}>Editar</a>
                                                </div>
                                                <div className="checkout-page">
                                                    <div className="checkout-form">
                                                        <form>
                                                            <div className="row">
                                                                <div className="col-lg-12 col-sm-12 col-xs-12">
                                                                    <div className="row check-out">
                                                                        <div className="form-group col-md-8 col-sm-12 col-xs-12">
                                                                            <div className="field-label">Contraseña antigua</div>
                                                                            <input className="form-control is-invalid" type="password" name="field-name" value={oldPass} placeholder="" onChange={(e) => setOldPass(e.target.value)} required aria-describedby="inputGroupPrepend3 validationOldPassFeedback"/>
                                                                            {
                                                                                !validOldPass ?
                                                                                <div id="validationOldPassFeedback" className="invalid-feedback">
                                                                                    Ingrese una contraseña de 9 caracteres o mas.
                                                                                </div>
                                                                                : null
                                                                            }
                                                                        </div>
                                                                        <div className="form-group col-md-8 col-sm-12 col-xs-12">
                                                                            <div className="field-label">Contraseña nueva</div>
                                                                            <input className="form-control is-invalid" type="password" name="field-name" value={newPass} placeholder="" onChange={(e) => setNewPass(e.target.value)} required aria-describedby="inputGroupPrepend3 validationNewPassFeedback"/>
                                                                            {
                                                                                !validNewPass ?
                                                                                <div id="validationNewPassFeedback" className="invalid-feedback">
                                                                                    Ingrese una contraseña de 9 caracteres o mas.
                                                                                </div>
                                                                                : null
                                                                            }
                                                                            {
                                                                                !validEqualsPass ?
                                                                                <div id="validationNewPassFeedback" className="invalid-feedback">
                                                                                    La nueva contraseña no coincide con la confirmacion.
                                                                                </div>
                                                                                : null
                                                                            }
                                                                        </div>
                                                                        <div className="form-group col-md-8 col-sm-12 col-xs-12">
                                                                            <div className="field-label">Confirmar contraseña nueva</div>
                                                                            <input className="form-control is-invalid" type="password" name="field-name" value={confirmPass} placeholder="" onChange={(e) => setConfirmPass(e.target.value)} required aria-describedby="inputGroupPrepend3 validationConfirmPassFeedback"/>
                                                                            {
                                                                                !validConfirmPass ?
                                                                                <div id="validationConfirmPassFeedback" className="invalid-feedback">
                                                                                    Ingrese una contraseña de 9 caracteres o mas.
                                                                                </div>
                                                                                : null
                                                                            }
                                                                            {
                                                                                !validEqualsPass ?
                                                                                <div id="validationConfirmPassFeedback" className="invalid-feedback">
                                                                                    La nueva contraseña no coincide con la confirmacion.
                                                                                </div>
                                                                                : null
                                                                            }
                                                                        </div>
                                                                        {/* <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                                            <div className="field-label">Email</div>
                                                                            <input type="text" name="field-name" value={email} placeholder="" onChange={(e) => setEmail(e.target.value)} />
                                                                        </div> */}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <div className="row">
                                                <div className="col-10">
                                                    <div className="payment-box">
                                                        <div className="text-right"><a href="#" className="btn-solid btn" onClick={(e) => cancelPass(e)}>Cancelar</a></div>
                                                    </div>
                                                </div>
                                                <div className="col-2">
                                                    <div className="payment-box">
                                                        <div className="text-right"><a href="#" className="btn-solid btn" onClick={(e) => changePassword(e)}>Guardar</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    : null
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AccountInfo

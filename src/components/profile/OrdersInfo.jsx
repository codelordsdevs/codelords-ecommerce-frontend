import React, { useState } from 'react';
import {useDispatch} from 'react-redux';
import {setLoading} from '../../redux/loadingDucks';
import { useCookies } from "react-cookie";
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const OrdersInfo = (props) => {
    const dispatch = useDispatch();
    const [orders, setOrders] = useState([]);
    const [cookies, setCookie, removeCookie] = useCookies(["user"]);
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal)
    const [selectedOrder, setSelectedOrder] = useState([]);

    React.useEffect(() => {
		loadOrders();
	},[]);

    const loadOrders = async () => {
        dispatch(setLoading(1));
        const data = await fetch(`${process.env.REACT_APP_URL_API}/api/sale`, {
			method: 'OPTIONS',
			mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Bearer '+cookies.access_token,
                'Accept' : 'application/json',
            },
			body: JSON.stringify({
				'my_orders': 1,
				'user_id': props.user.id,
			}),
		});
        const result = await data.json();
        console.log(result)
        dispatch(setLoading(-1));
        setOrders(result);
    }

    const verDetalle = (e, item) => {
        e.preventDefault();
        setModal(!modal);
        console.log(item)
        setSelectedOrder(item);
    }
    
    return (
        <div className="col-lg-9">
            <div className="dashboard-right">
                <div className="dashboard">
                    <div className="box-account box-info">
                        <div className="box-head">
                            <h2>Información de compras</h2>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <div className="box-title">
                                        <h3>Lista de compras</h3>
                                    </div>
                                    <div className="box-content">
                                        <table className="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Numero de orden</th>
                                                    <th scope="col">Nombre</th>
                                                    <th scope="col">Email</th>
                                                    <th scope="col">Telefono</th>
                                                    <th scope="col">Estado</th>
                                                    <th scope="col">Detalle</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    orders.data && orders.data.length > 0 ?
                                                        orders.data.map((item) => (
                                                            <tr key={item.id}>
                                                                <th scope="row">{item.id}</th>
                                                                <td>{item.receiver_name}</td>
                                                                <td>{item.email}</td>
                                                                <td>{item.phone}</td>
                                                                <td>{item.sale_state.name}</td>
                                                                <td><a href="#" onClick={(e) => verDetalle(e, item)}>Ver detalle</a></td>
                                                            </tr>
                                                        ))
                                                    :null
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal isOpen={modal} toggle={toggle} >
                <ModalHeader toggle={toggle}>Detalle orden</ModalHeader>
                <ModalBody>
                    <div className="container">
                        {
                            selectedOrder && selectedOrder.sale_detail && selectedOrder.sale_detail.length > 0 ?
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Producto</th>
                                            <th scope="col">Cantidad</th>
                                            <th scope="col">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            selectedOrder.sale_detail.map((item) => (
                                                <tr key={item.id}>
                                                    <th scope="row">{item.product.name}</th>
                                                    <td>{item.qty}</td>
                                                    <td>{item.total_price}</td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </table>
                            : null
                            // cart.data && cart && cart.data.length > 0 ?
                            //     cart.data.map((item, index) => (
                            //         index < 3 ?
                            //         <div className="row border rounded mt-1">
                            //             <div className="col-4">
                            //                 <img 
                            //                     src={item.product.product_images != undefined && item.product.product_images.length > 0 ? item.product.product_images[0].url : null} 
                            //                     height='150px'>
                            //                 </img>
                            //             </div>
                            //             <div className='col-4 mt-5'>
                            //                 <h4>{item.product.name}</h4>
                            //                 <h4>{item.product.isOffer != undefined && item.product.isOffer.invert == 0 && item.product.isOffer.days > 0 ? '$' + item.product.formated_special_price : '$' + item.product.formated_price}</h4>
                            //             </div>
                            //             <div className='col-4 mt-5'>
                            //                 <h4>Cantidad</h4>
                            //                 <h4>{item.qty}</h4>
                            //             </div>
                            //         </div>
                            //         : null
                            //     ))
                            // : <div>Tu carrito esta vacío</div>
                        }
                    </div>
                </ModalBody>
            </Modal>
        </div>
    )
}

export default OrdersInfo

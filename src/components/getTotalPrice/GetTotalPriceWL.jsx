import React from "react";

const GetTotalPrice = (props) => {

  console.log(props)

  	const validarOferta = (isOffer) => {
      	if((isOffer.invert == 0) && (isOffer.y > 0 || isOffer.m > 0 || isOffer.d > 0 || isOffer.h > 0 || isOffer.i > 0)){
        	return 1
      	}
      	return 0
  	}

	const numberWithPoints = (number) => {
		return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
	}

  	let total = 0;

  	if (props.products && props.products.length > 0 ) {
    	for (let i = 0; i < props.products.length; i++) {
			if(props.products[i].selected){
				if(validarOferta(props.products[i].product.isOffer)) {
					total = ((props.products[i].product.special_price * props.products[i].qty ) + total);
				} else {
					total = ((props.products[i].product.price * props.products[i].qty ) + total);
				}
			}
			
        }
    }

  	return numberWithPoints('$' + total);
};

export default GetTotalPrice;

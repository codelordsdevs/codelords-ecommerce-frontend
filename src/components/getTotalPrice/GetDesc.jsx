import React from "react";

const GetTotalPrice = (props) => {

  console.log(props)


  let total = 0;

  if (props.products && props.products.length > 0 ) {
    for (let i = 0; i < props.products.length; i++) {
        
        total = ((props.products[i].product.price * props.products[i].qty ) + total);
        
    }
    
  }

  if (props.coupons && props.coupons.length > 0 ) {
    for (let i = 0; i < props.coupons.length; i++) {
        
        if(props.coupons[i].discount > 0){
            
           total  -= total * (props.coupons[i].discount/100)
        } 

    }
  }
  

  return total;
};

export default GetTotalPrice;
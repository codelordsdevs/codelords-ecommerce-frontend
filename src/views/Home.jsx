import React from 'react'
import HomeBanner from '../components/banners/HomeBanner'
import SecondaryBanner from '../components/banners/SecondaryBanner'
import FeaturedProducts from '../components/productsGroups/FeaturedProducts'

const Home = () => {

    return (
        <div>
            <HomeBanner />
            <FeaturedProducts title="productos especiales" api="featured"/>
            <SecondaryBanner />
            {/* <FeaturedProducts title="ofertas" api="discount"/>
            <FeaturedProducts title="novedades" api="news"/> */}
        </div>
    )
}

export default Home

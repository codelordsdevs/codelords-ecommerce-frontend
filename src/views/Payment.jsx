import React from "react";

import Shipment from "../components/payment/Shipment.jsx";
import { useCookies } from "react-cookie";
import { useHistory, Redirect } from "react-router";

const Payment = () => {

  const [products, setProduct] = React.useState([]);
  const [cookies, setCookie] = useCookies(["user"]);
	const [selectedCart,setSelectedCart] = React.useState([]);
		const history = useHistory();


  React.useEffect(() => {
    	  getProduct();
  },
  []
  );






  const coupons = [
    {
      id: 1,
      name: "EXAMPLECODE",
      code: "EXAMPLECODE",
      discount: 5,
      isPercent: 0,
    },
    {
      id: 2,
      name: "EXAMPLECODE2",
      code: "EXAMPLECODE2",
      discount: 10,
      isPercent: 1,
    },
  ];

	const getProduct = async () => {
		if(cookies.access_token != null){
			const data = await fetch(`${process.env.REACT_APP_URL_API}/api/cart`, {
				method: 'OPTIONS',
				mode: 'cors',
				headers: {
					'Content-Type': 'application/json',
					'Authorization' : 'Bearer '+cookies.access_token,
					'Accept' : 'application/json',
				},
				body: JSON.stringify({
					'selected' : 1
				}),
			});
    const db_products = await data.json();
      console.log(db_products)
			if(db_products.data != null && db_products.data.length > 0){
				setProduct(db_products.data);
				console.log('here')
				console.log(products)
			}else{
				history.push("/cart");
			}
		} 
	}



  return (
 <section class="section-b-space">
        <div class="container">
            <div class="checkout-page">
                <div class="checkout-form">
                    <form>
                        <div class="row">
                        <Shipment products={products} coupons={coupons}/>
                        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
       )						
}

export default Payment;

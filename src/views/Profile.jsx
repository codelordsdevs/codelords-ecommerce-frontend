import React from 'react';
import Breadcrumb from '../components/breadcrumb/Breadcrumb';
import { useCookies } from "react-cookie";
import { useHistory } from "react-router-dom";
import AccountInfo from '../components/profile/AccountInfo';
import OrdersInfo from '../components/profile/OrdersInfo';

const Profile = (props) => {
	const [cookies, setCookie, removeCookie] = useCookies(["user"]);
	const history = useHistory();
	const breadcrumbData = {
		title: 'Perfil',
		breadcrumbs: 
		[
			{
				id : "1",
				title : "INICIO",
				url : "/",
				active: "0"
			},
			{
				id : "2",
				title: "PERFIL",
				url: "/profile",
				active: "1"
			}
		]

	};

	const [user, setUser] = React.useState();
	const [userView, setUserView] = React.useState('account');
	const [selectedView, setSelectedView] = React.useState('account');

	React.useEffect(() => {
		getUserFromToken();
	},[]);

	const getUserFromToken = async () => {
		const data = await fetch(`${process.env.REACT_APP_URL_API}/api/auth/me`, {
			method: 'POST',
			mode: 'cors',
			cache: 'no-cache',
			headers: {
				'Authorization' : 'Bearer '+cookies.access_token,
				'Content-Type' : 'application/json',
				'Accept' : 'application/json',
			}
		});
		const login = await data.json();
		setUser(login);
	}
	
	const submit = async (e) => {
		const data = await fetch(`${process.env.REACT_APP_URL_API}/api/auth/logout`, {
			method: 'POST',
			mode: 'cors',
			cache: 'no-cache',
			headers: {
				'Authorization' : 'Bearer '+cookies.access_token,
				'Content-Type' : 'application/json'
			}
		});
		const login = await data.json();
		removeCookie("access_token");
		window.location.reload();
	}

	const changeView = (e, view) => {
		e.preventDefault();
		setSelectedView(view);
	}

    return (
	    <div>
		<Breadcrumb 
			title={breadcrumbData.title}
			breadcrumbs={breadcrumbData.breadcrumbs}
		/>
		<section className="section-b-space">
			<div className="container">
				<div className="row">
					<div className="col-lg-3">
						<div className="account-sidebar">
							<a className="popup-btn">My cuenta</a>
						</div>
						<div className="dashboard-left">
							<div className="collection-mobile-back">
								<span className="filter-back">
									<i className="fa fa-angle-left" aria-hidden="true"></i>Atras
								</span>
							</div>
							<div className="block-content">
								<ul>
									<li className="last">
										<a href="#" onClick={(e) => changeView(e, 'account')}>Información de cuenta</a>
									</li>
									{/* <li>
										<a href="#">Mis direcciones</a>
									</li> */}
									<li>
										<a href="#" onClick={(e) => changeView(e, 'orders')}>Mis pedidos</a>
									</li>
									<li className="last">
										<a href="#" onClick={submit}>Log Out</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					{
						selectedView == 'account' ? 
							<AccountInfo user={user} callUser={getUserFromToken}/>
						: selectedView == 'orders' ?
							<OrdersInfo user={user}/>
						: null
					}
					{/* <AccountInfo user={user} callUser={getUserFromToken}/>
					<OrdersInfo /> */}
				</div>
			</div>
		</section>
	</div>
    );
}

export default Profile;

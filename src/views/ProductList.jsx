import React from 'react'

import List from '../components/products/List'
import ProductFilter from '../components/products/ProductFilter'

const ProductList = () => {

    return (
        <section className="section-b-space ratio_asos">
            <div className="collection-wrapper">
                <div className="container">
                    <div className="row">
                        {/* aqui va la barra lateral */}
                        <ProductFilter />
                        <List />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ProductList

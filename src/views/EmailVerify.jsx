import React, { useState } from 'react';
import { useParams, Redirect } from "react-router-dom";

const EmailVerify = () => {
	const { id } = useParams();
	const { hash } = useParams();
	const verify = "true";
	const [response, setResponse] = useState([]);
	const [state, setState] = useState({
		redirect: false
	});

	React.useEffect(() => {
		getVerification();
	}, []);
	const getVerification = async () => {
		const data = await fetch(`${process.env.REACT_APP_URL_API}/api/user`, {
			method: "OPTIONS",
			mode: "cors",
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json",
			},
			body: JSON.stringify({
				verify,
				id,
				hash,
			}),
		});
		const verification = await data.json();
		setResponse(verification);
		return setTimeout(() => setState({ redirect: true }), 5000);
	};

	return state.redirect ?
		<Redirect to="/login" />
		:
		<div className="col-6 offset-3">
			<h1>{response.message}</h1>
		</div>
}

export default EmailVerify;

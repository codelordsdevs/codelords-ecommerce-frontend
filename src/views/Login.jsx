import React, { useState } from 'react';
import { useCookies } from "react-cookie";
import { useHistory } from "react-router-dom";
import {useDispatch} from 'react-redux';
import {setLoading} from '../redux/loadingDucks';

const Login = () => {
	const dispatch = useDispatch();
	const [cookies, setCookie] = useCookies(["user"]);
	const history = useHistory();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const submit = async (e) => {
		dispatch(setLoading(1));
		e.preventDefault();

		let addProductsInCart = false

		let cartStorage = JSON.parse(localStorage.getItem('cart'))
		if(cartStorage != undefined && cartStorage.length > 0){
			// if(window.confirm("\u00BF Su carrito de compras contiene productos, desea mantenerlos al iniciar sesion?")){
				addProductsInCart = true
			// }
		}

		const data = await fetch(`${process.env.REACT_APP_URL_API}/api/auth/login`, {
			method: 'POST',
			mode: 'cors',
			headers:
			{
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			},
			body: JSON.stringify({
				email,
				password,
			}),
		});

		const login = await data.json();
		if(login.message != 'Server Error'){
			if (login.status == true) {
				setCookie('access_token', login.access_token, { maxAge: login.expires_in });
				if(addProductsInCart){
					const cart = await fetch(`${process.env.REACT_APP_URL_API}/api/cart?emptyCart=1`, {
						method: 'OPTIONS',
						mode: 'cors',
						cache: 'no-cache',
						headers: {
							'Content-Type': 'application/json',
							'Authorization' : 'Bearer '+login.access_token,
							'Accept' : 'application/json',
						}
					});
					let cartStorage = JSON.parse(localStorage.getItem('cart'))
					
					for(let x = 0; x < cartStorage.length; x++){
						await fetch(`${process.env.REACT_APP_URL_API}/api/cart?product_id=${cartStorage[x].product.id}&qty=${cartStorage[0].qty}&user_id=1`,{
							method: "PUT",
							mode: "cors",
							cache: "no-cache",
							headers:{
							"Content-Type": "application/json",
							'Authorization' : 'Bearer '+login.access_token,
							'Accept' : 'application/json',
							},
						});
					}
				}
				window.location.reload();
			} else {
				window.alert('Credenciales Incorrectas porfavor intente nuevamente');
			}
		} else {
			dispatch(setLoading(-1));
			return alert('Error al intentar iniciar sesion')
		}
		dispatch(setLoading(-1));
	}
	
	return (

		<div>
			<section className="login-page section-b-space">
				<div className="container">
					<div className="row">
						<div className="col-lg-6">
							<h3>Iniciar Sesi&oacute;n</h3>
							<div className="theme-card">
								<form className="theme-form" onSubmit={submit}>
									<div className="form-group">
										<label htmlFor="email">Email</label>
										<input type="text" className="form-control" id="email" placeholder="Email" required=""
											onChange={e => setEmail(e.target.value)}
										/>
									</div>
									<div className="form-group">
										<label htmlFor="review">Contrase&ntilde;a</label>
										<input type="password" className="form-control" id="review" placeholder="Ingresese su contrase&ntilde;a" required=""
											onChange={e => setPassword(e.target.value)}
										/>
									</div>
									<input type="submit" className="btn btn-solid" value="Iniciar Sesi&oacute;n" />
								</form>
							</div>
						</div>
						<div className="col-lg-6 right-login">
							<h3>Nuevo Cliente</h3>
							<div className="theme-card authentication-right">
								<h6 className="title-font">Crear una cuenta</h6>
								<p>
									Cree una cuenta gratis en nuestra tienda. El registro es rapido y sencillo. Le permite mantener registro de sus pedidos en la tienda y poder comprar. Para comenzar a comprar haz click en Crear una Cuenta.
	    							</p>
								<a href="/register" className="btn btn-solid">Crear una cuenta</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	);
}

export default Login;

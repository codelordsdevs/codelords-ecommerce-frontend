import React from 'react';
import CartDetail from '../components/cartDetail/CartDetail';
import Breadcrumb from '../components/breadcrumb/Breadcrumb';


const Cart = () => {
	const breadcrumbData = {
		title: 'Carro de compras',
		breadcrumbs: 
		[
			{
				id : "1",
				title : "INICIO",
				url : "/",
				active: "0"
			},
			{
				id : "2",
				title: "Carro de compras",
				url: "/cart",
				active: "1"
			}
		]

	};

    return (
	    <div className="container">
			<Breadcrumb 
					title={breadcrumbData.title}
					breadcrumbs={breadcrumbData.breadcrumbs}
			/>
			<CartDetail />
	    </div>
    )
}

export default Cart

import React, { useState } from 'react';
import Breadcrumb from '../components/breadcrumb/Breadcrumb';
import { useDispatch } from 'react-redux';
import { setLoading } from '../redux/loadingDucks';

const Register = () => {
	const dispatch = useDispatch();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [name, setName] = useState('');
	const [lastname, setLastname] = useState('');

	const breadcrumbData = {
		title: 'Registro',
		breadcrumbs:
			[
				{
					id: '1',
					title: "INICIO",
					url: "/",
					active: "0"
				},
				{
					id: '2',
					title: "CREAR CUENTA",
					url: "/register",
					active: "1"
				}
			]
	};

	const submit = async (e) => {
		dispatch(setLoading(1));
		if (name == '') {
			dispatch(setLoading(-1));
			return alert('El nombre es un campo obligatorio para el registro.');
		}
		if (lastname == '') {
			dispatch(setLoading(-1));
			return alert('El apellido es un campo obligatorio para el registro.');
		}
		if (email == '') {
			dispatch(setLoading(-1));
			return alert('El email es un campo obligatorio para el registro.');
		} else {
			let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			if (!re.test(email)) {
				dispatch(setLoading(-1));
				return alert('Debe ingresar un correo electronico valido');
			}
		}
		if (password == '') {
			dispatch(setLoading(-1));
			return alert('La contraseña es un campo obligatorio para el registro.');
		}

		const data = await fetch(`${process.env.REACT_APP_URL_API}/api/user`, {
			method: 'PUT',
			mode: 'cors',
			headers:
			{
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			},
			credentials: 'include',
			body: JSON.stringify({
				email,
				password,
				name,
				lastname,
			}),
		});
		const register = await data.json();
		if (register.status && register.status === "true") {
			window.alert('usuario creado con exito, revise su correo para verificar su cuenta');
			window.location.replace('/login');
		} else {
			let messages = '';
			if (register.messages['email']) {
				messages += register.messages['email'] + '\r\n';
			}
			if (register.messages['password']) {
				messages += register.messages['password'] + '\r\n';
			}
			if (register.messages['name']) {
				messages += register.messages['name'] + '\r\n';
			}
			if (register.messages['lastname']) {
				messages += register.messages['lastname'] + '\r\n';
			}
			window.alert(messages);
		}
		dispatch(setLoading(-1));
	}
	return (
		<div>
			<Breadcrumb
				title={breadcrumbData.title}
				breadcrumbs={breadcrumbData.breadcrumbs}
			/>
			<section className="register-page section-b-space">
				<div className="container">
					<div className="row">
						<div className="col-lg-12">
							<h3>crear cuenta</h3>
							<div className="theme-card">
								<div className="theme-form">
									<div className="form-row">
										<div className="col-md-6">
											<label htmlFor="email">Nombre</label>
											<input type="text" className="form-control" id="fname" placeholder="Nombre" required=""
												onChange={e => setName(e.target.value)}
											/>
										</div>
										<div className="col-md-6">
											<label htmlFor="review">Apellido</label>
											<input type="text" className="form-control" id="lname" placeholder="Apellido" required=""
												onChange={e => setLastname(e.target.value)}
											/>
										</div>
									</div>
									<div className="form-row">
										<div className="col-md-6">
											<label htmlFor="email">email</label>
											<input type="text" className="form-control" id="email" placeholder="Email" required=""
												onChange={e => setEmail(e.target.value)}
											/>
										</div>
										<div className="col-md-6">
											<label htmlFor="review">Contrase&ntilde;a</label>
											<input type="password" className="form-control" id="review" placeholder="Ingrese su contrase&ntilde;a" required=""
												onChange={e => setPassword(e.target.value)}
											/>
										</div>
										<button onClick={submit} className="btn btn-solid">crear cuenta</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}

export default Register;

import { getHtmlToast } from '../redux/cartDucks'
import { toast } from 'react-toastify'

export const addProductLS = (product, qty) => {
    let newQty = qty != undefined && qty > 1 ? qty : 1
    let cart = localStorage.getItem('cart')
    if(cart != undefined){
        let arreglo = JSON.parse(cart)
        let existe = arreglo.filter((item) => item.product.id == product.id)
        if(existe.length > 0){
            toast(<div>El producto ya se encuentra en tu carrito</div>)
        } else {
            arreglo.push({product: product, qty: newQty})
            toast(getHtmlToast(product))
        }
        localStorage.setItem('cart', JSON.stringify(arreglo))        
    } else {
        let arreglo = []
        arreglo.push({product: product, qty: newQty, id: product.id})
        toast(getHtmlToast(product))
        localStorage.setItem('cart', JSON.stringify(arreglo))
    }
}

export const deleteProductLS = (product) => {
    let cart = localStorage.getItem('cart')
    if(cart != undefined){
        let arreglo = JSON.parse(cart)
        let newArray = arreglo.filter((item) => item.product.id != product.id)
        localStorage.setItem('cart', JSON.stringify(newArray))
    }
}

export const updateProductLS = (index, qty) => {
    let cart = localStorage.getItem('cart')
    if(cart != undefined){
        let arreglo = JSON.parse(cart)
        let newArray = arreglo.map((item, i) => {
            if(i== index){
                item.qty = parseInt(qty)
                return item
            } else {
                return item
            }
        })
        localStorage.setItem('cart', JSON.stringify(newArray))
    }
}

export const checkedProductLS = (index, selected) => {
    let cart = localStorage.getItem('cart')
    if(cart != undefined){
        let arreglo = JSON.parse(cart)
        let newArray = arreglo.map((item, i) => {
            if(i== index){
                item.selected = selected
                return item
            } else {
                return item
            }
        })
        localStorage.setItem('cart', JSON.stringify(newArray))
    }
}

export const deleteAllProductsLS = () => {
    let cart = localStorage.getItem('cart')
    if(cart != undefined){
        localStorage.removeItem('cart')
    }
}
import axios from 'axios'
import {setLoading} from './loadingDucks'

// constants
const initialData = {
    array: [],
    limit: 8,
    categorys: [],
    price_from: 0,
    price_to: 1000000
}

// types
const GET_ALL_PRODUCTS = 'GET_ALL_PRODUCTS'
const GET_PRODUCTS_FILTERED = 'GET_PRODUCTS_FILTERED'
const EXPAND_LIMIT = 'EXPAND_LIMIT'
const UPDATE_CATEGORYS = 'UPDATE_CATEGORYS'
const CHANGE_PRICE_RANGE = 'CHANGE_PRICE_RANGE'
const SEARCH_PRODUCT = 'SEARCH_PRODUCT'

// reducer
export default function productReducer(state = initialData, action) {
    switch(action.type){
        case GET_ALL_PRODUCTS:
            return {...state, array: action.payload, limit: 8}
        case GET_PRODUCTS_FILTERED:
            return {...state, array: action.payload, limit: 8, categorys: action.categorys}
        case EXPAND_LIMIT:
            return {...state, limit: action.payload}
        case UPDATE_CATEGORYS:
            return {...state, categorys: action.categorys }
        case CHANGE_PRICE_RANGE:
            return {
                ...state, 
                categorys: action.categorys, 
                price_from: action.price_from, 
                price_to: action.price_to 
            }
        case SEARCH_PRODUCT:
            return {...state, array: action.payload, limit: 8}
        default:
            return state
    }
}

// actions
export const getAllProductsAction = () => async (dispatch, getState) => {
    dispatch(setLoading(1))
    try {
        const res = await axios.get(`${process.env.REACT_APP_URL_API}/api/product`)
        dispatch({
            type: GET_ALL_PRODUCTS,
            payload: res.data.data
        })
        dispatch(setLoading(-1))
    } catch(error) {
        dispatch(setLoading(-1))
        console.log(error)
    }
}

export const getProductFiltered = () => async (dispatch, getState) => {
    dispatch(setLoading(1))
    try {
        let categorysString = getState().product.categorys.join()
        let price_from = getState().product.price_from
        let price_to = getState().product.price_to

        if(categorysString.length == 0){
            const res = await axios.options(`${process.env.REACT_APP_URL_API}/api/product?price_from=${price_from}&price_to=${price_to}`)
            dispatch({
                type: GET_ALL_PRODUCTS,
                payload: res.data.data,
                categorys: getState().product.categorys
            })
        } else {
            const res = await axios.options(`${process.env.REACT_APP_URL_API}/api/product?category_id=${categorysString}&price_from=${price_from}&price_to=${price_to}`)
            dispatch({
                type: GET_ALL_PRODUCTS,
                payload: res.data.data,
                categorys: getState().product.categorys
            })
        }
        dispatch(setLoading(-1))
    } catch(error) {
        dispatch(setLoading(-1))
        console.log(error)
    }
}

export const expandLimit = (actualLimit) => async (dispatch, getState) => {
    try {
        let newLimit = actualLimit + 8
        dispatch({
            type: EXPAND_LIMIT,
            payload: newLimit
        })
    } catch(error) {
        console.log(error)
    }
}

export const addCategoryFilter = (category) => async (dispatch, getState) => {
    try {
        let categorys = getState().product.categorys
        let newCategorys = [...categorys, category]
        dispatch({
            type: UPDATE_CATEGORYS,
            categorys: newCategorys,
            price_from: getState().product.price_from,
            price_to: getState().product.price_to
        })
        getProductFiltered()
    } catch(error) {
        console.log(error)
    }
}

export const deleteCategoryFilter = (category) => async (dispatch, getState) => {
    try {
        const categorys = getState().product.categorys.filter((item, index) => item !== category)
        // const index = categorys.indexOf(category)
        // console.log(categorys)
        // console.log(index)
        // const newCategorys = categorys.splice(2, 1)
        dispatch({
            type: UPDATE_CATEGORYS,
            categorys: categorys,
            price_from: getState().product.price_from,
            price_to: getState().product.price_to
        })
        getProductFiltered()
    } catch(error) {
        console.log(error)
    }
}

export const changePriceRange = (price_from, price_to) => async (dispatch, getState) => {
    try {
        let categorys = getState().product.categorys
        dispatch({
            type: CHANGE_PRICE_RANGE,
            categorys: categorys,
            price_from: price_from,
            price_to: price_to
        })
    } catch(error) {
        console.log(error)
    }
}

export const searchProduct = (value) => async (dispatch, getState) =>{
    dispatch(setLoading(1))
    try {
        const res = await axios.options(`${process.env.REACT_APP_URL_API}/api/product?search=${value}`)
        dispatch({
            type: SEARCH_PRODUCT,
            payload: res.data.data
        })
        dispatch(setLoading(-1))
    } catch(error) {
        dispatch(setLoading(-1))
        console.log(error)
    }
}
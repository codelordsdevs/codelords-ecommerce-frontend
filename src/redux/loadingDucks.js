// constant
const initialData = {
    loading: 0
}

// types
const SET_LOADING = 'SET_LOADING'

// reducer
export default function loadingReducer(state = initialData, action){
    switch(action.type){
        case SET_LOADING:
            return {...state, loading: action.payload}
        default:
            return state
    }
}

// actions
export const setLoading = (value) => async (dispatch, getState) => {
    try{
        let actualValue = getState().loading.loading
        let newValue = actualValue + value
        dispatch({
            type: SET_LOADING,
            payload: newValue
        })
    }catch(error){
        console.log(error)
    }
}
// constants
const initialData = {
    array: {
        'firstName': true,
        'lastName': true,
        'email': true,
        'phone': true,
        'rut': true,
        'region': true,
        'comuna': true,
        'address': true
    }
}

// types
const GET_VALIDATIONS = 'GET_VALIDATIONS'

// reducer
export default function paymentReducer(state = initialData, action) {
    switch(action.type){
        case GET_VALIDATIONS:
            return {...state, array: action.payload}
        default:
            return state
    }
}

// actions
export const setValidations = (validaciones) => async (dispatch, getState) => {
    try {
        dispatch({
            type: GET_VALIDATIONS,
            payload: validaciones
        })
    } catch(error) {
        console.log(error)
    }
}
import axios from 'axios'
import {setLoading} from './loadingDucks'

// constants
const initialData = {
    one: {}
}

// types
const GET_CONFIG = 'GET_CONFIG'

// reducer
export default function configReducer(state = initialData, action){
    switch(action.type) {
        case GET_CONFIG:
            return {...state, one: action.payload}
        default:
            return state
    }
}

// actions
export const getConfig = () => async (dispatch, getState) => {
    dispatch(setLoading(1))
    try{
        const res = await axios.get(`${process.env.REACT_APP_URL_API}/api/config`)
        if(res.data.data !== undefined && res.data.data.length > 0){
            const config = res.data.data[0]
            //Object.assign(config)
            dispatch({
                type: GET_CONFIG,
                payload: config
            })
        }
        dispatch(setLoading(-1))
    } catch(error){
        dispatch(setLoading(-1))
        console.log(error)
    }
}
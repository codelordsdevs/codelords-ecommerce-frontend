import { ToastContainer, toast } from 'react-toastify';

// constants
const initialData = {
    elements: 0
}

// types
const ADD_PRODUCT_IN_CART = 'ADD_PRODUCT_IN_CART'
const DELETE_PRODUCT_IN_CART = 'DELETE_PRODUCT_IN_CART'
const DELETE_ALL_CART = 'DELETE_ALL_CART'

// reducer
export default function cartReducer(state = initialData, action) {
    switch(action.type){
        case ADD_PRODUCT_IN_CART:
            return {...state, elements: action.payload}
        case DELETE_PRODUCT_IN_CART:
            return {...state, elements: action.payload}
        case DELETE_ALL_CART:
            return {...state, elements: action.payload}
        default:
            return state
    }
}

// action
export const addProductInCartAction = (product) => async (dispatch, getState) => {
    const products = getState().cart.elements + 1
    // toast(getHtmlToast(product))
    dispatch({
        type: ADD_PRODUCT_IN_CART,
        payload: products
    })
}

export const deleteProductInCartAction = (product) => async (dispatch, getState) => {
    const products = getState().cart.elements -1
    dispatch({
        type: DELETE_PRODUCT_IN_CART,
        payload: products
    })
}

export const deleteAllCartAction = () => async (dispatch, getState) => {
    const newElements = 0
    dispatch({
        type: DELETE_ALL_CART,
        payload: newElements
    })
}

export const getHtmlToast = (product) => {
    return <div className='row'>
        <div className='col-4'>
            <img 
                src={product.product_images.length > 0 ? product.product_images[0].url : null} 
                height='50px'>
            </img>
        </div>
        <div className='col-8'>
            {product.name} <br></br>
            {product.isOffer.invert == 0 && product.isOffer.days > 0 ? '$' + product.formated_special_price : '$' + product.formated_price}
        </div>
    </div>
}

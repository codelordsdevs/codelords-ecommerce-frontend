import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import configReducer from './configDucks'
import loadingReducer from './loadingDucks'
import productReducer from './productDucks'
import cartReducer from './cartDucks'
import paymentReducer from './paymentDucks'

const rootReducer = combineReducers({
    product: productReducer,
    config: configReducer,
    loading: loadingReducer,
    cart: cartReducer,
    payment: paymentReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore() {
    const store = createStore( rootReducer, composeEnhancers( applyMiddleware(thunk) ) )
    return store
}
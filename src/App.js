import React, { useRef } from "react";
import MessengerCustomerChat from 'react-messenger-customer-chat';

import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect,
} from "react-router-dom";

import Footer from "./components/globalElements/Footer";
import Cart from "./views/Cart";
import Home from "./views/Home";
import Product from "./views/Product";
import Products from "./views/Products";
import Search from "./views/Search";
import Login from "./views/Login";
import Register from "./views/Register";
import Profile from "./views/Profile";
import EmailVerify from './views/EmailVerify';
import Payment from './views/Payment';
import OrderStatus from './views/OrderStatus';


import { useCookies } from "react-cookie";
import ProductList from "./views/ProductList";
import Header from "./components/header/Header";
import { Provider } from 'react-redux';
import generateStore from './redux/store';
import Loading from "./components/globalElements/Loading";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AccountPayment from "./views/AccountPayment";
import PaymentWL from "./views/PaymentWL";

function App() {

	const [cookies, setCookie] = useCookies(["user"]);
	const [user, setUser] = React.useState();

	const store = generateStore()
	const childRef = useRef()
	React.useEffect(() => {
		getUserFromToken()
	}, []);

	const breadcrumbData = {
		title: 'Perfil',
		breadcrumbs:
			[
				{
					title: "INICIO",
					url: "/",
					active: "0"
				},
				{
					title: "PERFIL",
					url: "/profile",
					active: "1"
				}
			]

	};

	const getUserFromToken = async () => {
		const data = await fetch(`${process.env.REACT_APP_URL_API}/api/auth/me`, {
			method: 'POST',
			mode: 'cors',
			cache: 'no-cache',
			headers: {
				'Authorization': 'Bearer ' + cookies.access_token,
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			}
		});
		const login = await data.json();
		setUser(login);
		childRef.current.getInitialConfig()
	}


	return (
		<Provider store={store}>
			<Loading />
			<MessengerCustomerChat
				pageId="105591718136896"
				appId="269107518155346"
			/>
			<Router>
				<Header ref={childRef} />
				<Switch>
					<Route path="/paymentwl">
						<PaymentWL />
					</Route>
					<Route path="/accountPayment">
						{/* <AccountPayment /> */}
						{
							!cookies.access_token ?
								<AccountPayment />
								:
								<Redirect to="/profile" />
						}
					</Route>
					<Route path="/productlist">
						<ProductList />
					</Route>
					<Route path="/product/:id">
						<Product />
					</Route>
					<Route path="/cart">
						<Cart />
						{/* {
						cookies.access_token ?
							<Cart />	
						: 
							<Redirect to="/login" />
					} */}
					</Route>
					<Route path="/products">
						<Products />
					</Route>
					<Route path="/search">
						<Search />
					</Route>
					<Route path="/login">
						{
							!cookies.access_token ?
								<Login />
								:
								<Redirect to="/profile" />
						}
					</Route>
					<Route path="/register">
						{
							!cookies.access_token ?
								<Register />
								:
								<Redirect to="/profile" />
						}
					</Route>
					<Route path="/profile">
						{
							cookies.access_token ?
								<Profile />
								:
								<Redirect to="/login" />
						}
					</Route>
					<Route path="/verify/:id/:hash">
						<EmailVerify />
					</Route>
					<Route path="/payment">
						{
							cookies.access_token ?
								<Payment />
								:
								<Redirect to="/login" />
						}
					</Route>
					<Route path="/orderstatus/:id">
						{
							<OrderStatus />
						}
					</Route>
					<Route path="/">
						<Home />
					</Route>
				</Switch>
				<Footer />
			</Router>
			<ToastContainer
				position="top-right"
				autoClose={3000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover={false}
			/>
		</Provider>
	);
}

export default App;
